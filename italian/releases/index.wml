#use wml::debian::template title="Versioni di Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ccf6e79eefe5b0ce09c41d5b657645e86214a9f3" maintainer="Luca Monducci"

<p>Debian contiene sempre almeno tre versioni che sono mantenute in maniera
attiva: <q>stable</q> (stabile), <q>testing</q> (in fase di test) e
<q>unstable</q> (non stabile).</p>

<dl>
<dt><a href="stable/">stable</a></dt>
 <dd>
 <p>
  La distribuzione <q>stable</q> contiene l'ultima versione
  ufficialmente rilasciata di Debian.
 </p>
 <p>
  È quella che deve essere usata per gli ambienti di produzione, quella che
  raccomandiamo di usare.
 </p>
 <p>
  L'attuale distribuzione <q>stable</q> di Debian è la versione
  <:=substr '<current_initial_release>', 0, 2:> ed è chiamata
  <em><current_release_name></em>.
<ifeq "<current_initial_release>" "<current_release>"
  "È stata rilasciata il <current_release_date>."
/>
<ifneq "<current_initial_release>" "<current_release>"
  "La versione iniziale <current_initial_release> è stata rilasciata
  il <current_initial_release_date> e l'ultimo aggiornamento, versione
  <current_release>, è stato rilasciato il <current_release_date>."
/>
 </p>
 </dd>

<dt><a href="testing/">testing</a></dt>
 <dd>
 <p>
  La distribuzione <q>testing</q> contiene i pacchetti che non sono ancora
  stati accettati nella <q>stable</q> ma che sono nella coda per il passaggio.
  Il maggior vantaggio nell'utilizzare questa distribuzione è che include
  versioni più aggiornate del software.
 </p>
 <p>
  Si veda la <a href="$(DOC)/manuals/debian-faq/">Debian FAQ</a> per maggiori informazioni su
  <a href="$(DOC)/manuals/debian-faq/ftparchives#testing">cosa sia <q>testing</q></a>
  e <a href="$(DOC)/manuals/debian-faq/ftparchives#frozen">come diventa <q>stable</q></a>.
 </p>
 <p>
  L'attuale distribuzione <q>testing</q> è chiamata <em><current_testing_name></em>.
 </p>
 </dd>

<dt><a href="unstable/">unstable</a></dt>
 <dd>
 <p>
  La distribuzione <q>unstable</q> è quella in cui si sta portando avanti lo
  sviluppo di Debian. Di norma, questa distribuzione è usata dagli
  sviluppatori che amano avere sempre tutto aggiornato. Si raccomanda agli
  utenti che utilizzano unstable di sottoscrivere la lista di messaggi
  debian-devel-announce per ricevere le notifiche delle principali
  modifiche, per esempio degli aggiornamenti che potrebbero creare dei
  problemi.
</p>
 </p>
 <p>
  La versione <q>unstable</q> è sempre chiamata <em>sid</em>.
 </p>
 </dd>
</dl>

<h2>Ciclo di vita dei rilasci</h2>
<p>Debian annuncia il nuovo rilascio stabile a cadenza regolare.
Gli utenti possono aspettarsi di avere 3 anni di supporto completo per
ciascun rilascio oltre 2 anni di supporto LTS aggiuntivo.
</p>

<p>Nelle pagine wiki dei <a
href="https://wiki.debian.org/DebianReleases">Rilasci Debian</a>
e <a href="https://wiki.debian.org/LTS">Debian LTS</a> sono disponibili
informazioni dettagliate.
</p>

<h2>Indice dei rilasci</h2>

<ul>

  <li><a href="<current_testing_name>/">La prossima versione di Debian
    sarà chiamata <q><current_testing_name></q></a>
      &mdash; <q>testing</q>, la data di rilascio non è stata stabilita
      <!-- &mdash; rilascio previsto il 6 luglio 2019</li> -->

  <li><a href="buster/">Debian 10 (<q>buster</q>)</a>
      &mdash; l'attuale versione <q>stable</q></li>
  <li><a href="stretch/">Debian 9 (<q>stretch</q>)</a>
      &mdash; l'attuale versione  <q>oldstable</q> dispone del
	  <a href="https://wiki.debian.org/LTS">supporto LTS</a></li>
  <li><a href="jessie/">Debian 8 (<q>jessie</q>)</a>
      &mdash; l'attuale versione <q>oldoldstable</q> dispone
	  dell'<a href="https://wiki.debian.org/LTS/Extended">estensione
	  del supporto LTS</a></li>
<!--      &mdash; una vecchia versione stabile</li> -->
  <li><a href="wheezy/">Debian 7 (<q>wheezy</q>)</a>
      &mdash; una vecchia versione stabile</li>
  <li><a href="squeeze/">Debian 6.0 (<q>squeeze</q>)</a>
      &mdash; una vecchia versione stabile</li>
  <li><a href="lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a>
      &mdash; una vecchia versione stabile</li>
  <li><a href="etch/">Debian GNU/Linux 4.0 (<q>etch</q>)</a>
      &mdash; una vecchia versione stabile</li>
  <li><a href="sarge/">Debian GNU/Linux 3.1 (<q>sarge</q>)</a>
      &mdash; una vecchia versione stabile</li>
  <li><a href="woody/">Debian GNU/Linux 3.0 (<q>woody</q>)</a>
      &mdash; una vecchia versione stabile</li>
  <li><a href="potato/">Debian GNU/Linux 2.2 (<q>potato</q>)</a>
      &mdash; una vecchia versione stabile</li>
  <li><a href="slink/">Debian GNU/Linux 2.1 (<q>slink</q>)</a>
      &mdash; una vecchia versione stabile</li>
  <li><a href="hamm/">Debian GNU/Linux 2.0 (<q>hamm</q>)</a>
      &mdash; una vecchia versione stabile</li>
</ul>

<p>Le pagine web per le vecchie versioni sono mantenute online intatte, ma la
release è mantenuta in un <a href="$(HOME)/distrib/archive">archivio
separato</a>.</p>

<p>Si veda la <a href="$(HOME)/doc/manuals/debian-faq/">Debian FAQ</a> per una spiegazione
dell'<a href="$(HOME)/doc/manuals/debian-faq/ftparchives#sourceforcodenames">origine
dei nomi in codice</a>.</p>

<h2>Integrità dei dati in un rilascio</h2>

<p>L'integrità dei dati è assicurata da un file <code>Release</code> che
viene elettronicamente firmato. Per assicurarsi che tutti i file di un
rilascio vi siano inclusi, viene copiata il checksum dei vari file
<code>Packages</code> all'interno del file <code>Release</code>.</p>

<p>Le firme digitali di questo file sono incluse in un file chiamato
<code>Release.gpg</code>, che usa la versione corrente della chiave
per la firma dell'archivio. Per <q>stable</q> e <q>oldstable</q>
esiste una firma aggiuntiva creata usando una chiave speciale generata
appositamente per il rilascio da un membro dello
<a href="$(HOME)/intro/organization#release-team">Stable Release
Team</a>.</p>
