#use wml::debian::template title="Veraltete Dokumentation"
#use wml::debian::translation-check translation="4792632f7a20682a368627c66663b57ff1b3fa8b"
# $Id$
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/obsolete.defs"
# Translator: Helge Kreutzmann <debian@helgefjell.de>, 2007-06-30
# Updated: Holger Wansing <linux@wansing-online.de>, 2012.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2020.

<h1 id="historical">Historische Dokumente</h1>

<p>Die unten aufgeführten Dokumente wurden entweder vor langer Zeit geschrieben
   und sind nicht mehr aktuell, oder wurden für vorhergehende Versionen von
   Debian geschrieben und für aktuelle Versionen nicht mehr aktualisiert. Ihre
   Informationen sind veraltet, könnten aber für Einige noch von Interesse sein.
</p>

<p>Für Dokumente, die nicht mehr relevant sind und keinen Zweck mehr erfüllen,
   wurden die Verweise gelöscht, aber der Quellcode vieler solcher Dokumente
   ist noch unter
   <a href="https://salsa.debian.org/ddp-team/attic">DDP's attic</a> zu finden.
</p>

<h2 id="user">Benutzer-orientierte Dokumentation</h2>

<document "dselect-Beschreibung für Anfänger" "dselect">

<div class="centerblock">
<p>
  Diese Datei beschreibt Dselect für diejenigen, die das Programm zum
  ersten Mal benutzen. Es soll Ihnen dabei helfen, Debian erfolgreich
  zu installieren. Es wird nicht versucht, alles zu erklären. Wenn
  Sie also das erste Mal Dselect benutzen, arbeiten Sie sich durch die
  Hilfe-Bildschirme.
</p>
<doctable>
  <authors "Stéphane Bortzmeyer">
  <maintainer "(?)">
  <status>
  angehalten: <a href="https://packages.debian.org/aptitude">aptitude</a>
  hat dselect als Standardschnittstelle für die Debian-Paketverwaltung
  ersetzt
  </status>
  <availability>
  <inddpvcs name="dselect-beginner" formats="html txt pdf ps"
            langs="ca cs da de en es fr hr it ja pl pt ru sk" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Benutzer-Leitfaden" "users-guide">

<div class="centerblock">
<p>
  Dieser <q>Benutzer-Leitfaden</q> ist nichts anderes als ein neu formatierter
  <q>Progeny Benutzer-Leitfaden</q>. Die Inhalte wurden an das Standard
  Debian-System angepasst.</p>

<p>Über 300 Seiten mit guten Anleitungen, um damit anzufangen, das
   Debian-System von einem <acronym lang="en" title="Graphical User Interface">
   GUI</acronym>-Desktop und einer Shell-Kommandozeile aus zu verwenden.
</p>
<doctable>
  <authors "Progeny Linux Systems, Inc.">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  Selbstständig nützlich als Anleitung. Für die Woody-Veröffentlichung
  geschrieben, wird obsolet.
  </status>
  <availability>
# langs="en" isn't redundant, it adds the needed language suffix to the link
  <inddpvcs name="users-guide" index="users-guide" langs="en" formats="html txt pdf" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Tutorial" "tutorial">

<div class="centerblock">
<p>
  Dieses Handbuch ist als Einführung für neue Benutzer gedacht, wenn
  GNU/Linux installiert ist, oder für neue Benutzer auf einem
  Linux-System, das von anderen gewartet wird.
</p>
<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II">
  <editors "Havoc Pennington">
  <maintainer "(?)">
  <status>
  angehalten; unvollständig; wahrscheinlich von der
  <a href="user-manuals#quick-reference">Debian-Referenz</a> überholt
  </status>
  <availability>
  noch nicht vollständig
  <inddpvcs name="debian-tutorial" vcsname="tutorial" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian GNU/Linux: Guide to Installation and Usage" "guide">

<div class="centerblock">
<p>
  Ein Handbuch, das sich am Endbenutzer orientiert.
</p>
<doctable>
  <authors "John Goerzen, Ossama Othman">
  <editors "John Goerzen">
  <status>
  fertig (jedoch ist es für Potato)
  </status>
  <availability>
  <inoldpackage "debian-guide">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Anwenderreferenzhandbuch" "userref">

<div class="centerblock">
<p>
  Dieses Handbuch bietet zumindest einen Überblick über alles, was ein
  Benutzer über ein Debian-GNU/Linux-System wissen sollten (wie das
  Aufsetzen von X, die Konfiguration des Netzes, der Zugriff auf
  Disketten usw.). Es ist dazu gedacht, die Brücke zwischen dem
  Debian-Tutorial und den detaillierten Handbuch- und Info-Seiten,
  die in jedem Paket enthalten sind, zu schlagen.</p>

  <p>Es ist ebenfalls dazu gedacht, eine Idee zu übermitteln, wie
  Befehle kombiniert werden, streng nach dem Unix-Prinzip, dass <em>es
  immer mehrere Möglichkeiten gibt, zum Ziel zu kommen</em>.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer">
  <editors "Thalia L. Hooker, Oliver Elphick">
  <maintainer "(?)">
  <status>
  angehalten und ziemlich unvollständig; wahrscheinlich von der
  <a href="user-manuals#quick-reference">Debian-Referenz</a> überholt
  </status>
  <availability>
  <inddpvcs name="user" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian System-Administrator-Handbuch" "system">

<div class="centerblock">
<p>
  Dieses Dokument wird in der Einführung des Policy-Handbuchs
  erwähnt. Es bespricht alle Aspekte der Administration eines
  Debian-Systems.
</p>
<doctable>
  <authors "Tapio Lehtonen">
  <maintainer "(?)">
  <status>
  angehalten; unvollständig; wahrscheinlich von der
  <a href="user-manuals#quick-reference">Debian-Referenz</a> überholt
  </status>
  <availability>
  noch nicht verfügbar
  <inddpvcs name="system-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Netzwerk-Administrator-Handbuch" "network">

<div class="centerblock">
<p>
  Dieses Handbuch diskutiert alle Aspekte der Netzwerkadministration
  eines Debian-Systems.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson, Ivan E. Moore II">
  <maintainer "(?)">
  <status>
  angehalten; unvollständig; wahrscheinlich von der
  <a href="user-manuals#quick-reference">Debian-Referenz</a> überholt
  </status>
  <availability>
  noch nicht verfügbar
  <inddpvcs name="network-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

# Add this to books, there's a revised edition (2nd) @ Amazon
<document "Das Linux-Kochbuch" "linuxcookbook">

<div class="centerblock">
<p>
  Ein greifbarer Referenzleitfaden zum Debian GNU/Linux-System, das in über
  1.500 <q>Rezepten</q> zeigt, wie es bei tagtäglichen Aktivitäten verwendet
  wird &ndash; von der Arbeit mit Texten, Bildern und Klängen bis zu
  Produktivität und Netzwerk-Fragen. Wie die Software, die das Buch beschreibt,
  ist auch das Buch selbst <q>copyleft</q> und der Quellcode dazu verfügbar.
</p>
<doctable>
  <authors "Michael Stutz">
  <status>
  veröffentlicht; für Woody geschrieben, wird obsolet
  </status>
  <availability>
  <inoldpackage "linuxcookbook">
  <p><a href="http://dsl.org/cookbook/">vom Autor</a></p>
  </availability>
</doctable>
</div>

<hr />

<document "APT HOWTO" "apt-howto">

<div class="centerblock">
<p>
  Dieses Handbuch versucht eine kurze, aber dennoch vollständige Quelle für
  Informationen über das APT-System und seine Fähigkeiten zu sein. Es
  enthält viele Informationen über den Einsatz von APT sowie viele Beispiele.
</p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>
  überholt seit 2009
  </status>
  <availability>
  <inoldpackage "apt-howto">
  <inddpvcs name="apt-howto" langs="en ca cs de es el fr it ja pl pt-br ru uk tr zh-tw zh-cn"
	    formats="html txt pdf ps" naming="locale" />
  </availability>
</doctable>
</div>

<h2 id="devel">Entwickler-Dokumentation</h2>

<document "Einführung: Erstellung eines Debian-Pakets" "makeadeb">

<div class="centerblock">
<p>
  Eine Einführung, wie ein Debian-Paket mit Hilfe von
  <strong>debmake</strong> erstellt wird.
</p>
<doctable>
  <authors "Jaldhar H. Vyas">
  <status>
  angehalten, veraltet durch das <a href="devel-manuals#maint-guide">Debian-Leitfaden
  für Neue Paketbetreuer</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">HTML Online</a>
  </availability>
</doctable>
</div>

<hr />

<document "Handbuch für Debian-Programmierer" "programmers">

<div class="centerblock">
<p>
  Hilft neuen Entwicklern dabei, ein Paket für Debian GNU/Linux zu erstellen.
</p>
<doctable>
  <authors "Igor Grobman">
  <status>
  veraltet durch das <a href="devel-manuals#maint-guide">Debian-Leitfaden
  für Neue Paketbetreuer</a>
  </status>
  <availability>
  <inddpvcs name="programmer" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian Paketierungs-Handbuch" "packman">

<div class="centerblock">
<p>
  Dieses Handbuch beschreibt die technischen Aspekte bei der Erzeugung
  von Binär- und Quellpaketen für Debian. Es dokumentiert die
  Schnittstelle zwischen dselect und den Skripten der Access-Methoden.
  Es behandelt nicht die Policy-Anforderungen und es setzt
  Vertrautheit mit Funktionen von dpkg aus der Perspektive eines
  Systemadministrators voraus.
</p>

<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  Teile, die de facto Policy waren, wurden kürzlich in
  <a href="devel-manuals#policy">debian-policy</a> aufgenommen.
  </status>
  <availability>
  <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>

<hr />

<document "Wie Software-Hersteller ihre Produkte direkt im .deb-Format bereitstellen können" "swprod">

<div class="centerblock">
<p>
  This document is intended as a starting point to explain how software
  producers can integrate their products with Debian, what different
  situations can arise depending on the license of the products and the
  choices of the producers, and what possibilities there are. It does not
  explain how to create packages, but it links to documents which do exactly
  that.

  <p>You should read this if you are not familiar with the big picture of
  creating and distributing Debian packages, and optionally with adding them
  to the Debian distribution.

<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  veraltet
  </status>
  <availability>
  <inddpvcs-distribute-deb>
  </availability>
</doctable>
</div>

<hr />

<document "Einführung in i18n" "i18n">

<div class="centerblock">
<p>
  Dieses Handbuch beschreibt die Grundidee von l10n (Lokalisierung),
  i18n (Internationalisierung) und m17n (Multilingualisierung) sowohl
  für Entwickler als auch für Paketbetreuer.
</p>

<p>
  Der Sinn dieses Dokuments besteht darin, mehr Pakete mit
  Unterstützung für i18n zu erhalten und die Debian-Distribution
  internationaler zu machen. Mitarbeiter aus der gesamten Welt sind
  herzlich willkommen, da der ursprüngliche Autor Japaner ist und
  dieses Dokument Japanisierung beschreiben würde, wenn niemand
  mitarbeitet.
</p>

<doctable>
  <authors "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <maintainer "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <status>
  eingestellt, veraltet
  </status>
  <availability>
  noch nicht fertig
  <inddpvcs-intro-i18n>
  </availability>
</doctable>
</div>

<hr />

<document "Debian SGML/XML Howto" "sgml-howto">

<div class="centerblock">
<p>
  This HOWTO contains practical information about the use of SGML and XML
  on a Debian operating system.

<doctable>
  <authors "Stephane Bortzmeyer">
  <maintainer "Stephane Bortzmeyer">
  <status>
  eingestellt, veraltet
  </status>
  <availability>

# English only using index.html, so langs set.
  <inddpvcs name="sgml-howto"
            formats="html"
            srctype="SGML"
            vcstype="attic"
/>
  </availability>
</doctable>
</div>

<hr />

<document "Debian XML/SGML Policy" "xml-sgml-policy">

<div class="centerblock">
<p>
  Teil-Policy für Debian-Pakete, die XML- oder SGML-Ressourcen zur Verfügung
  stellen und/oder diese verwenden.
</p>

<doctable>
  <authors "Mark Johnson, Ardo van Rangelrooij, Adam Di Carlo">
  <status>
  am Anfang, die aktuelle SGML-Policy von <tt>sgml-base-doc</tt> und neues
  Material zur XML-Katalog-Verwaltung wird eingearbeitet.
  </status>
  <availability>
  <inddpvcs-xml-sgml-policy>
  </availability>
</doctable>
</div>

<hr />

<document "DebianDoc-SGML-Markup-Handbuch" "markup">

<div class="centerblock">
<p>
  Dokumentation für das <strong>debiandoc-sgml</strong>-System, enthält
  die besten Vorgehensweisen und Tipps für Betreuer. Zukünftige
  Versionen sollten Tipps für die einfachere Wartung sowie das
  Erstellen von Dokumentation in Debian-Paketen, Richtlinien für die
  Organisation von Übersetzungen und andere sinnvolle Informationen
  enthalten. Lesen Sie dazu auch <a
  href="https://bugs.debian.org/43718">Fehler #43718</a>.

<doctable>
  <authors "Ian Jackson, Ardo van Rangelrooij">
  <maintainer "Ardo van Rangelrooij">
  <status>
  fertig (?)
  </status>
  <availability>
  <inpackage "debiandoc-sgml-doc">
  <inddpvcs-debiandoc-sgml-doc>
  </availability>
</doctable>
</div>

<h2 id="misc">Verschiedene Dokumentation</h2>

<document "Debian-Depot-HOWTO" "repo">

<div class="centerblock">
<p>
  Dieses Dokument beschreibt was ein Debian-Depot ist, wie es funktioniert, wie
  Sie selbst ein solches aufbauen können und wie Sie dies korrekt in die
  <tt>sources.list</tt> eintragen.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  fertig (?)
  </status>
  <availability>
  <inddpvcs name="repository-howto" index="repository-howto"
            formats="html" langs="en fr de uk ta">
  </availability>
</doctable>
</div>
