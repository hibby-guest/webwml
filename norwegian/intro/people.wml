#use wml::debian::template title="Folk: hvem vi er, hva vi gjør"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="bbb1b67054b9061c9420f1c5826b2fa85f05c738" maintainer="Hans F. Nordhaug"

# translators: some text is taken from /intro/about.wml

<h2>Utviklere og bidragsytere</h2>

#include "about_sub_who.data"

<p>Den komplette listen med offisielle Debian-medlemmer fins på
<url https://nm.debian.org/members> hvor medlemskap håndteres.
En bredere liste over Debian-bidragsytere fins på
<url https://contributors.debian.org>.</p>

<h3><a name="history">Hvordan startet det hele?</a></h3>

#include "about_sub_history.data"
  
<h2>Individer og organisasjoner som støtter Debian</h2>

<p>Mange andre individer og organisasjoner er del av Debian-fellesskapet:</p>

<ul>
  <li><a href="https://db.debian.org/machines.cgi">Netthotell- og maskinvaresponsorer</a></li>
  <li><a href="../mirror/sponsors">Filspeilsponsorer</a></li>
  <li><a href="../partners/">Utviklings- og servicepartnere</a></li>
  <li><a href="../consultants">Konsulenter</a></li>
  <li><a href="../CD/vendors">Leverandører av Debian installasjonsmedium</a></li>
  <li><a href="distrib/pre-installed">Datamaskinleverandører som forhåndsinstallerer Debian</a></li>
  <li><a href="../events/merchandise">Vareleverandører</a></li>
</ul>

<h2><a name="users">Hvem bruker Debian?</a></h2>

<p>Selv om ingen nøyaktig statistikk er tilgjengelig (siden Debian
ikke krever at brukerne registrerer seg), så er det ganske sterke bevis
for at Debian brukes av et bredt spekter av organisasjoner, store og små,
i tillegg til mange tusen individer. Se vår 
<a href="../users/">Hvem bruker Debian?</a>-side for en liste med
høyt profilerte organisasjoner som har sendt inn korte beskrivelser av 
hvordan og om hvorfor de bruker Debian.
