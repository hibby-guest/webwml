msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:10+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr ""

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr ""

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "trenutni"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "cxlan"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:41
#, fuzzy
msgid "Stable Release Manager"
msgstr "Upravitelj izdaje"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr ""

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
#, fuzzy
msgid "chair"
msgstr "predsedujocxi"

#: ../../english/intro/organization.data:48
#, fuzzy
msgid "assistant"
msgstr "Upravitelj izdaje"

#: ../../english/intro/organization.data:50
#, fuzzy
msgid "secretary"
msgstr "Tajnik"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Funkcionarji"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distribucija"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
#, fuzzy
msgid "Publicity team"
msgstr "Odnosi z javnostjo"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "Podpora in infrastruktura"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Vodja"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Tehnicxna komisija"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Tajnik"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Razvojni projekti"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "Arhive FTP"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr ""

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr ""

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Upravljanje izdaje"

#: ../../english/intro/organization.data:138
#, fuzzy
msgid "Release Team"
msgstr "Upravitelj izdaje"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Zagotavljanje kakovosti"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Skupina za namestitveni sistem"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:150
#, fuzzy
msgid "Release Notes"
msgstr "Upravitelj izdaje"

#: ../../english/intro/organization.data:152
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "CD Images"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Produkcija"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "Testiranje"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:167
#, fuzzy
msgid "Autobuilding infrastructure"
msgstr "Podpora in infrastruktura"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:176
#, fuzzy
msgid "Buildd administration"
msgstr "Sistemska administracija"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "Dokumentacija"

#: ../../english/intro/organization.data:198
#, fuzzy
msgid "Work-Needing and Prospective Packages list"
msgstr "Seznam nadebudnih in dela potrebnih paketov"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "Predstavnik za tisk"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "Spletne strani"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:238
#, fuzzy
msgid "Debian Women Project"
msgstr "Razvojni projekti"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "Dogodki"

#: ../../english/intro/organization.data:264
#, fuzzy
msgid "DebConf Committee"
msgstr "Tehnicxna komisija"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "Partnerski program"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr ""

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "Sledilnik napak"

#: ../../english/intro/organization.data:320
#, fuzzy
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Administracija posxtnih seznamov"

#: ../../english/intro/organization.data:329
#, fuzzy
msgid "New Members Front Desk"
msgstr "Sprejemnica za nove vzdrzxevalce"

#: ../../english/intro/organization.data:335
#, fuzzy
msgid "Debian Account Managers"
msgstr "Upravitelji uporabnisxkih imen razvijalcev"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Vzdrzxevalci kljucxev (PGP in GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "Skupina za varnost"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "Pravilnik"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "Sistemska administracija"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Na ta naslov, lahko posxljete sporocxilo, cxe naletite na tezxavo na katerem "
"od Debianovih racxunalnikov, vkljucxno s tezxavami z gesli in paketi, ki jih "
"potrebujete."

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Cxe imate strojne tezxave z Debianovimi racxunalniki, glejte stran <a href="
"\"https://db.debian.org/machines.cgi\">Debianovi racxunalniki</a>, kjer "
"boste nasxli podatke o administratorjih posameznih racxunalnikov."

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "Skrbnik imenika razvijalcev LDAP"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "Zrcalni strezxniki"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "Vzdrzxevalec DNSa"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "Sledilnikov paketov"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:392
#, fuzzy
msgid "Salsa administrators"
msgstr "Sistemska administracija"

#~ msgid "Individual Packages"
#~ msgstr "Posamezni paketi"

#, fuzzy
#~| msgid "Installation System Team"
#~ msgid "Live System Team"
#~ msgstr "Skupina za namestitveni sistem"

#~ msgid "Publicity"
#~ msgstr "Odnosi z javnostjo"

#, fuzzy
#~ msgid "Custom Debian Distributions"
#~ msgstr "Distribucija"

#~ msgid "Release Manager for ``stable''"
#~ msgstr "Upravitelj izdaje ''stable''"

#~ msgid "Vendors"
#~ msgstr "Prodajalci"

#~ msgid "APT Team"
#~ msgstr "Skupina za APT"

#~ msgid "Mailing List Archives"
#~ msgstr "Arhive posxtnih seznamov"

#, fuzzy
#~ msgid "Security Testing Team"
#~ msgstr "Skupina za varnost"

#~ msgid "Mailing list"
#~ msgstr "Posxtni seznam"

#~ msgid "Installation"
#~ msgstr "Namestitev"

#~ msgid "Delegates"
#~ msgstr "Delegati"

#, fuzzy
#~ msgid "Installation System for ``stable''"
#~ msgstr "Skupina za namestitveni sistem"

#, fuzzy
#~| msgid "Security Team"
#~ msgid "Testing Security Team"
#~ msgstr "Skupina za varnost"

#, fuzzy
#~ msgid "Alioth administrators"
#~ msgstr "Sistemska administracija"

#~ msgid "User support"
#~ msgstr "Podpora uporabnikov"

#~ msgid "Firewalls"
#~ msgstr "Pozxarni zidovi"

#~ msgid "Laptops"
#~ msgstr "Prenosni racxunalniki"

#~ msgid "Special Configurations"
#~ msgstr "Posebne konfiguracije"

#~ msgid "Ports"
#~ msgstr "Druge platforme"

#, fuzzy
#~ msgid "CD Vendors Page"
#~ msgstr "Prodajalci"

#~ msgid "Consultants Page"
#~ msgstr "Svetovalska stran"
