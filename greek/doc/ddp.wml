#use wml::debian::ddp title="Το Σχέδιο Τεκμηρίωσης του Debian"
#use wml::debian::translation-check translation="66a989955b4ffb2ffa1af62f99ac4b75d99f6cf5" maintainer="galaxico"

<p>Το Σχέδιο Τεκμηρίωσης του Debian σχηματίστηκε για να συντονίσει και να 
ενοποιήσει όλες τις προσπάθειες για να γραφτεί περισσότερη και καλλίτερη 
τεκμηρίωση για το σύστημα του Debian.</p>

  <h2>Έργο του DDP</h2>
<div class="line">
  <div class="item col50">

    <h3>Εγχειρίδια</h3>
    <ul>
      <li><strong><a href="user-manuals">Εγχειρίδια χρηστών</a></strong></li>
      <li><strong><a href="devel-manuals">Εγχειρίδια 
προγραμματιστ(ρι)ών</a></strong></li>
      <li><strong><a href="misc-manuals">Διάφορα εγχειρίδια</a></strong></li>
      <li><strong><a href="#other">Προβληματικά εγχειρίδια</a></strong></li>
    </ul>

    <h3>Τρέχοντα θέματα</h3>
    <ul>
      <li><a href="topics">Θέματα</a> προς συζήτηση ή επίλυση.</li>

    </ul>

    <h3>Λίστα με αυτά που πρέπει να γίνουν (Todo)</h3>
    <ul>
      <li><a href="todo">πράγματα που πρέπει να γίνουν</a>· 
εθελοντές/εθελόντριες πάντα ευπρόσδεκτοι/ες!
        </li>
      <li><a href="todo#ideas">Καλές και κακές ιδέες</a>, μεγαλύτερα 
ζητήματα που πρέπει να επιλυθούν.</li>
    </ul>

  </div>

  <div class="item col50 lastcol">
      
    <h3>Πολιτική τεκμηρίωσης</h3>
    <ul>
      <li>Οι άδειες χρήσης των εγχειριδίων συμμορφώνονται με τις 
Κατευθύνσεις του Debian για το Ελεύθερο Λογισμικό (DFSG).</li>
      <li>Για τα κείμενά μας χρησιμοποιούμε το Docbook XML.</li>
      <li>Ο πηγαίος κώδικας πρέπει να είναι στον σύνδεσμο <a 
href="https://salsa.debian.org/ddp-team">https://salsa.debian.org/ddp-team</a></
li>
      <li>Ο επίσημος σύνδεσμος 
θα είναι ο <tt>www.debian.org/doc/&lt;manual-name&gt;</tt></li>
      <li>Κάθε κείμενο θα πρέπει να συντηρείται ενεργά.</li>
      <li>Παρακαλούμε ρωτήστε στη λίστα <a 
href="https://lists.debian.org/debian-doc/">debian-doc</a> αν θέλετε να 
γράψετε ένα καινούριο κείμενο</li>
    </ul>

    <h3>Πρόσβαση στο Git</h3>
    <ul>
      <li><a href="vcs">Πώς να αποκτήσετε πρόσβαση</a> στο αποθετήριο git του 
Σχεδίου τεκμηρίωσης DDP</li>
    </ul>

  </div>


</div>

<hr class="clr">

<p>Σημείωση για τους χρήστες που μιλούν Πορτογαλικά: επισκεφθείτε τις 
ιστοσελίδες στον σύνδεσμο <a href="http://wiki.debianbrasil.org/">DDP-BR</a>, 
σχετικά με την τοπικοποίηση της τεκμηρίωσης του Debian στα Βραζιλιάνικα 
Πορτογαλικά.</p>

<hr />

<h2><a name="other">Προβληματικά εγχειρίδια</a></h2>

<p>Επιπρόσθετα από τα συνήθως προβαλλόμενα εγχειρίδια, διατηρούμε επίσης τα 
παρακάτω εγχειρίδια που είναι, με τον ένα ή τον άλλο τρόπο, προβληματικά, οπότε 
δεν μπορούμε να τα συστήσουμε στους χρήστες. 
are problematic in one way or another, so we can't recommend them to all
users. Caveat emptor (Σας προειδοποιήσαμε!).</p>

<ul>
  <li><a href="obsolete#tutorial">Debian Tutorial</a>, έχει αντικατασταθεί από 
τον
      <a href="obsolete#guide">Οδηγό του Debian</a></li>
  <li><a href="obsolete#userref">Εγχειρίδιο Αναφοράς του Χρήστη του Debian</a>,
      στάσιμο και αρκετά ελλιπές</li>
  <li><a href="obsolete#system">Εγχειρίδιο του Διαχειριστή Συστήματος Debian 
</a>, στάσιμο, σχεδόν άδειο</li>
  <li><a href="obsolete#network">Εγχειρίδιο του Διαχειριστή Δικτύου του Debian 
</a>, στάσιμο, ελλιπές</li>
  <li><a href="devel-manuals#swprod">How Software Producers can distribute
      their products directly in .deb format</a>, στάσιμο, ξεπερασμένο</li>
  <li><a href="devel-manuals#packman">Εγχειρίδιο παραγωγής πακέτων του Debian 
</a>, μερικά συγχωνευμένο στο
      <a href="devel-manuals#policy">Εγχειρίδιο Πολιτικής του Debian</a>,
      το υπόλοιπο θα συμπεριληφθεί σε ένα εγχειρίδιο αναφοράς του dpkg που 
ακόμα δεν έχει γραφτεί</li>
  <li><a href="obsolete#makeadeb">Introduction: Making a Debian
      Package</a>, αντικαταστάθηκε από τον
      <a href="devel-manuals#maint-guide">Οδηγό Νέων Συντηρητών του 
Debian</a></li>
  <li><a href="obsolete#programmers">Εγχειρίδιο Προγραμματιστ(ρι)ών του 
Debian</a>,
      αντικαταστάθηκε από τον 
      <a href="devel-manuals#maint-guide">Οδηγό Νέων Συντηρητών του Debian</a>
      και τα
      <a href="devel-manuals#debmake-doc">Οδηγός για Συντηρητές του 
Debian</a></li>
  <li><a href="obsolete#repo">Debian Repository HOWTO</a>, ξεπερασμένα μετά 
την εισαγωγή του ασφαλούς APT</li>
</ul>
