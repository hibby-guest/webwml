#use wml::debian::translation-check translation="122cd4d0371114e8cb5f8eb2f4d7b3d228204e1e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le moteur web
webkit2gtk :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8625">CVE-2019-8625</a>

<p>Sergei Glazunov a découvert qu'un contenu web contrefait pourrait
conduire à un script intersite universel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8720">CVE-2019-8720</a>

<p>Wen Xu a découvert qu'un contenu web contrefait pourrait conduire
à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8769">CVE-2019-8769</a>

<p>Pierre Reimertz a découvert que la visite d'un site web contrefait peut
révéler l'historique de navigation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8771">CVE-2019-8771</a>

<p>Eliya Stein a découvert qu'un contenu web contrefait peut violer la
politique de bac à sable d'iframe.</p></li>

</ul>

<p>Pour la distribution stable (buster), ces problèmes ont été corrigés
dans la version 2.26.1-3~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4558.data"
# $Id: $
