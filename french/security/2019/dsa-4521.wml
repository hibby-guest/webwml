#use wml::debian::translation-check translation="47e98befc5737223dae4aac04b97842526b5c5da" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Trois vulnérabilités de sécurité ont été découvertes dans
l'environnement d'exécution de conteneurs Docker : le chargement non
sécurisé de bibliothèques NSS dans <q>docker cp</q> pourrait avoir pour
conséquence l'exécution de code avec les droits du superutilisateur, des
données sensibles pourraient être enregistrées dans le mode débogage et il
y avait une vulnérabilité d'injection de commande dans la commande
<q>docker build</q>.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 18.09.1+dfsg1-7.1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets docker.io.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de docker.io, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/docker.io">\
https://security-tracker.debian.org/tracker/docker.io</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4521.data"
# $Id: $
