#use wml::debian::translation-check translation="cdcf3bccf115a6d16bbc80940463b8f456ccc51b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Michal Bentkowski a découvert que ruby-sanitize, un nettoyeur de HTML
basé sur une liste d'éléments autorisés, est prédisposé à une
vulnérabilité de contournement de nettoyage HTML lors de l'utilisation de
la configuration <q>relaxed</q> ou une configuration personnalisée
permettant certains éléments. Le contenu dans un élément &lt;math&gt; ou
&lt;svg&gt; peut ne pas être nettoyé correctement même si math et svg ne
sont pas dans la liste des éléments autorisés.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 4.6.6-2.1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-sanitize.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-sanitize,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-sanitize">\
https://security-tracker.debian.org/tracker/ruby-sanitize</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4730.data"
# $Id: $
