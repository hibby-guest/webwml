#use wml::debian::translation-check translation="9366447bdb2abdccc3962d87981f5cce43bf471c" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
webkit2gtk :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3885">CVE-2020-3885</a>

<p>Ryan Pickren a découvert qu'une URL de fichier peut être traitée
incorrectement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3894">CVE-2020-3894</a>

<p>Sergei Glazunov a découvert qu'une situation de compétition peut
permettre à une application de lire de la mémoire sensible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3895">CVE-2020-3895</a>

<p>grigoritchy a découvert que le traitement de contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3897">CVE-2020-3897</a>

<p>Brendan Draper a découvert qu'un attaquant distant pourrait être en
mesure de provoquer l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3899">CVE-2020-3899</a>

<p>OSS-Fuzz a découvert qu'un attaquant distant pourrait être en mesure de
provoquer l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3900">CVE-2020-3900</a>

<p>Dongzhuo Zhao a découvert que le traitement de contenu web contrefait
pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3901">CVE-2020-3901</a>

<p>Benjamin Randazzo a découvert que le traitement de contenu web
contrefait pourrait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3902">CVE-2020-3902</a>

<p>Yigit Can Yilmaz a découvert que le traitement de contenu web contrefait
pourrait conduire à une attaque par script intersite.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.28.2-2~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4681.data"
# $Id: $
