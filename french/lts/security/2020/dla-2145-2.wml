#use wml::debian::translation-check translation="76e08d4de53f01be99018f9784d6e15237a4234b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de régression pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une régression introduite par la DLA-2145-1
due à une application incorrecte du correctif amont pour les CVE-2020-10108 &amp;
CVE-2020-10109 concernant un certain nombre de vulnérabilités de fractionnement
de requête HTTP dans Twisted, un cadriciel en Python basé sur les évènements
pour construire différents types d’applications web.

<p>Merci à Etienne Allovon pour son rapport détaillé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10108">CVE-2020-10108</a>

<p>Dans Twisted Web jusqu’à 19.10.0, il existait une vulnérabilité de
fractionnement de requête HTTP. Lorsque deux en-têtes content-length sont
fournis, le premier en-tête est ignoré. Lorsque la seconde valeur content-length
était réglée à zéro, le corps de la requête était interprété comme une requête
redirigée (pipe).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10109">CVE-2020-10109</a>

<p>Dans Twisted Web jusqu’à 19.10.0, il existait une vulnérabilité de
fractionnement de requête HTTP. Lorsqu’un en-tête content-length et un encodage
de transfert en bloc étaient fournis, le content-length prévalait et la suite du
corps de la requête était interprétée comme une requête redirigée (pipe).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 14.0.2-3+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets twisted.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2145-2.data"
# $Id: $
