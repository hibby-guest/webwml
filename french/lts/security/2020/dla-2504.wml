#use wml::debian::translation-check translation="f9b4be408e2d0dda899c4974d0a22334341da7a5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans MediaWiki, un moteur
de site web pour travail collaboratif.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15005">CVE-2020-15005</a>

<p>Des wikis privés derrière un serveur de cache utilisant la fonctionnalité
de sécurité d’authentification d’image img_auth.php ont pu avoir leurs fichiers
mis en cache publiquement, aussi n’importe quel utilisateur non autorisé
pourrait les voir.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35477">CVE-2020-35477</a>

<p>Blocage d’essais légitimes de dissimulation d’entrées de journaux dans
certaines situations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35479">CVE-2020-35479</a>

<p>XSS possible à l’aide de BlockLogFormatter.php. Langage::translateBlockExpiry
lui-même ne protège pas dans tous les chemins du code. Par exemple, la valeur
renvoyée de Langage::userTimeAndDate est toujours non sécurisée pour l’HTML dans
une valeur de mois.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35480">CVE-2020-35480</a>

<p>Des utilisateurs absents (comptes non existants) et des utilisateurs cachés
(comptes qui ont été explicitement dissimulés à cause de leur comportement
inadapté) que le lecteur ne peut pas voir, sont gérés différemment, exposant
des informations sensibles sur l’état caché à des lecteurs non privilégiés.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1:1.27.7-1~deb9u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mediawiki.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mediawiki, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mediawiki">https://security-tracker.debian.org/tracker/mediawiki</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2504.data"
# $Id: $
