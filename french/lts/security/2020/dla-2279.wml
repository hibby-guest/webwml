#use wml::debian::translation-check translation="bb3f804000185056dc659c88709d05facf94aa3f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans la servlet Tomcat et le
moteur JSP.</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9484">CVE-2020-9484</a>

<p>Lors de l’utilisation d’Apache Tomcat, et que : a) un attaquant pouvait contrôler le
contenu et le nom d’un fichier sur le serveur, b) le serveur était configuré pour
utiliser le PersistenceManager avec un FileStore, c) le PersistenceManager était
configuré avec sessionAttributeValueClassNameFilter="null" (par défaut à moins
qu’un SecurityManager n’était utilisé) ou un filtre suffisamment laxiste pour
permettre à l’attaquant de fournir un objet à désérialiser et d) l’attaquant
connaissait un chemin relatif de fichier vers l’emplacement de stockage utilisé
par FileStore pour le fichier dont l’attaquant avait le contrôle, alors, à l’aide
d’une requête spécialement contrefaite, l’attaquant était capable de déclencher
une exécution de code à distance à l’aide d’une désérialisation du fichier sous
son contrôle. Il est à remarquer que toutes les conditions devaient être remplies
pour la réussite de l’attaque.</p>

</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11996">CVE-2020-11996</a>

<p>Une séquence contrefaite pour l'occasion de requêtes HTTP/2 envoyées à Apache
Tomcat pouvaient déclencher un usage immodéré de CPU pendant plusieurs secondes.
Si un nombre suffisamment grand de telles requêtes étaient faites sur des
connexions concurrentes HTTP/2, le serveur pouvait ne pas réagir.</p></li>

</ul>

<p>Pour Debian 9 <q>stretch</q>, ces problèmes ont été corrigés dans
la version 8.5.54-0+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat8.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat8, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat8">https://security-tracker.debian.org/tracker/tomcat8</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2279.data"
# $Id: $
