#use wml::debian::translation-check translation="ed01f92afd077896b85601388d54d14e9f1783c7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de régression pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une régression dans la dernière mise à jour
de Django, le cadriciel de développement web en Python. Le correctif de l’amont
pour CVE-2020-13254 pour les divulgations à l’aide de clés memcached mal formées
pourrait, dans certaines situations, causer une trace de la pile.</p>

<p>Veuillez consulter <tt>https://code.djangoproject.com/ticket/31654</tt> pour
plus d’informations.</p>

<ul>
<li>
<a href="https://security-tracker.debian.org/tracker/CVE-2020-13254">
CVE-2020-13254 : divulgation potentielle de données à l’aide de clés memcached
mal formées.
</a>

<p>Dans le cas où un dorsal memcached ne réalise pas une validation de clé,
le passage de clés mal formées en cache pourrait aboutir à une collision de clés
et une divulgation potentielle de données. Pour éviter cela, une validation de
clé a été ajoutée dans les dorsaux memcached de cache.</p>
</li>
</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.7.11-1+deb8u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2233-2.data"
# $Id: $
