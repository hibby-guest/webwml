#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans le moteur de
servlet et JSP de Tomcat.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1304">CVE-2018-1304</a>

<p>Le modèle d’URL de "" (la chaîne vide) qui correspond exactement au contexte
du superutilisateur, n’était pas géré correctement dans Tomcat d’Apache
lorsqu’utilisé comme partie de la définition de restriction. Cela faisait que la
restriction était ignorée. Il était, par conséquent, possible pour des
utilisateurs non autorisés d’acquérir l’accès aux ressources de l’application
web qui auraient dû être protégées. Seules les restrictions de sécurité avec
comme modèle d’URL la chaîne vide sont touchées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1305">CVE-2018-1305</a>

<p>Des restrictions de sécurité définies par des annotations de servlet dans
Tomcat d’Apache étaient seulement appliquées que lorsqu’un servlet était chargé.
À cause des restrictions de sécurité définies de cette façon appliquées au modèle
d’URL et n’importe quelle URL en découlant, il était possible — en fonction
de l’ordre de chargement des servlets — que quelques restrictions de sécurité
ne soient appliquées. Cela pourrait avoir exposé des ressources à des
utilisateurs n’ayant pas de droit d’accès.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 8.0.14-1+deb8u12.</p>
<p>Nous vous recommandons de mettre à jour vos paquets tomcat8.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1450.data"
# $Id: $
