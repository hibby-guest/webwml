#use wml::debian::translation-check translation="c5e08c849464686937f8349a835513c27137b65f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans wavpack, telles qu’une lecture
hors limites (qui pourrait éventuellement permettre une attaque par déni de
service), un flot de contrôle inattendu, des plantages, un dépassement d'entier
ou une erreur de segmentation.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 5.0.0-2+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wavpack.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wavpack, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wavpack">https://security-tracker.debian.org/tracker/wavpack</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2525.data"
# $Id: $
