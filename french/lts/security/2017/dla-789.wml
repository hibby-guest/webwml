#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Grégoire Scano"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Brève introduction</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5208">CVE-2017-5208</a>

<p>Choongwoo Han a signalé [0] un plantage exploitable dans wrestool de
icoutils. Les outils de ligne de commande sont utilisés, par exemple,
dans l'analyse de métadonnées de KDE.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5331">CVE-2017-5331</a>

<p>Il s'est avéré que la correction pour
<a href="https://security-tracker.debian.org/tracker/CVE-2017-5208">CVE-2017-5208</a>
n'était pas suffisante et qu'une correction supplémentaire était nécessaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5332">CVE-2017-5332</a>

<p>Mais il existe toujours des combinaisons d'arguments qui
font que le test est positif même si le bloc mémoire identifié par
le décalage de size n'est pas entièrement dans la mémoire total_size.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5333">CVE-2017-5333</a>

<p>La vérification de la mémoire n'était pas assez stricte sur les systèmes 64 bits.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.29.1-5deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets icoutils.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-789.data"
# $Id: $
