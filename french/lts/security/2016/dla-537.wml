#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Roundcube, une solution web de messagerie pour les serveurs IMAP, était
vulnérable à des vulnérabilités de script intersite (XSS) lors du
traitement d'images SVG. Lors d'un clic droit sur le lien de téléchargement
d'une image attachée, il était possible que du Javascript incorporé
puisse être exécuté dans un onglet distinct.</p>

<p>La mise à jour désactive l'affichage d'images SVG dans les courriels et
les onglets. Le téléchargement des pièces jointes est toujour possible.
Cette mise à jour de sécurité atténue aussi d'autres manières d'exploiter
ce problème  dans les images SVG.
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-4068">CVE-2016-4068</a>)</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.7.2-9+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets roundcube.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-537.data"
# $Id: $
