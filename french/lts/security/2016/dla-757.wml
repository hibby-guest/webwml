#use wml::debian::translation-check translation="5a92f5ba5c86bcac9b588a3e7656db8f48163007" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Divers problèmes de sécurité ont été découverts et corrigés dans
phpMyAdmin dans Wheezy.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4412">CVE-2016-4412</a> / PMASA-2016-57

<p>Un utilisateur peut être piégé à suivre un lien menant à phpMyAdmin,
qui, après authentification, redirige vers un autre site malveillant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6626">CVE-2016-6626</a> / PMASA-2016-49

<p>Dans le correctif pour PMASA-2016-57, il n'y avait pas suffisamment de
vérifications et il était possible de contourner la liste blanche.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9849">CVE-2016-9849</a> / PMASA-2016-60

<p>Contournement des règles <q>deny</q> (AllowRoot &amp; Others) en
utilisant un octet Null.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9850">CVE-2016-9850</a> / PMASA-2016-61

<p>La correspondance de nom d'utilisateur pour les règles <q>allow/deny</q>
peut avoir pour conséquence des mauvaises correspondances et détections du
nom d'utilisateur dans la règle du fait d'un temps d'exécution non
constant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9861">CVE-2016-9861</a> / PMASA-2016-66

<p>Dans le correctif pour PMASA-2016-49, il y avait des vérifications
boguées et il était possible de contourner la liste blanche.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9864">CVE-2016-9864</a> / PMASA-2016-69

<p>Plusieurs vulnérabilités d'injection SQL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9865">CVE-2016-9865</a> / PMASA-2016-70

<p>Du fait d'un bogue dans l'analyse d'une chaîne sérialisée, il était
possible de contourner la protection offerte par la fonction
PMA_safeUnserialize().</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4:3.4.11.1-2+deb7u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets phpmyadmin.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-757.data"
# $Id: $
