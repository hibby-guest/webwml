#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans PostgreSQL, un
système de base de données SQL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5423">CVE-2016-5423</a>

<p>Karthikeyan Jambu Rajaraman a découvert que les expressions CASE-WHEN
imbriquées ne sont pas correctement évaluées, menant éventuellement à un
plantage ou permettant la divulgation de portions de la mémoire du serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5424">CVE-2016-5424</a>

<p>Nathan Bossart a découvert que les caractères spéciaux dans la base de
données et les noms de rôle ne sont pas correctement gérés, menant
éventuellement à l'exécution de commandes avec les droits du
super-utilisateur quand il exécute pg_dumpall ou d'autres opérations de
maintenance de routine.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 9.1.23-0+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets postgresql-9.1.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-592.data"
# $Id: $
