#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans tardiff :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-0857">CVE-2015-0857</a>

<p>L'exécution de commande arbitraire était possible grâce à des
métacaractères de l'interpréteur dans le nom (1) d'un fichier tar ou (2)
d'un fichier à l'intérieur d'un fichier tar.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-0858">CVE-2015-0858</a>

<p>Des utilisateurs locaux pourraient écrire dans des fichiers arbitraires
à l'aide d'une attaque par lien symbolique sur un nom de chemin dans un
répertoire temporaire /tmp/tardiff-$$.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.1-1+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tardiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-564.data"
# $Id: $
