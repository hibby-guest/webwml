#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités de sécurité ont été découvertes dans libuser, une
bibliothèque qui implémente une interface standardisée pour manipuler et
administrer les comptes d'utilisateurs et de groupes, qui pourraient
conduire à un déni de service ou à une élévation de privilèges par des
utilisateurs locaux.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-3245">CVE-2015-3245</a>

<p>Une vulnérabilité de liste noire incomplète dans la fonction chfn dans
libuser avant 0.56.13-8 et 0.60 avant 0.60-7, tel qu'utilisé dans le
programme userhelper dans le paquet usermode, permet à des utilisateurs
locaux de provoquer un déni de service (corruption de /etc/passwd) à l'aide
d'un caractère de changement de ligne dans le champ GECOS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-3246">CVE-2015-3246</a>

<p>libuser avant 0.56.13-8 et 0.60 avant 0.60-7, tel qu'utilisé dans le
programme userhelper dans le paquet usermode, modifie directement
/etc/passwd. Cela permet à des utilisateurs locaux de provoquer un déni de
service (état de fichier inconsistant) en provoquant une erreur durant la
modification. ATTENTION : ce problème peut être combiné au
<a href="https://security-tracker.debian.org/tracker/CVE-2015-3245">CVE-2015-3245</a>
pour obtenir des privilèges.</p></li>

</ul>

<p>En complément, le paquet usermode, qui dépend de libuser, a été
reconstruit avec la version mise à jour.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans</p>

<p>libuser 1:0.56.9.dfsg.1-1.2+deb7u1 et usermode  1.109-1+deb7u2</p>

<p>Nous vous recommandons de mettre à jour vos paquets libuser et usermode.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-468.data"
# $Id: $
