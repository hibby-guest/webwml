#use wml::debian::ddp title="Le Projet de documentation Debian"
#use wml::debian::translation-check translation="60eea6916091f1a3a9240f472ebea1904c32d0bd" maintainer="Jean-Paul Guillonneau"

# Premier traducteur: Frédéric Bothamy, 2008

<p>Le Projet de documentation Debian (DDP en anglais) a été créé pour
coordonner et unifier tous les travaux fournis pour documenter le
système Debian.</p>

  <h2>Travaux du DDP</h2>
<div class="line">
  <div class="item col50">

    <h3>Manuels</h3>
    <ul>
      <li><strong><a href="user-manuals">Manuels pour les utilisateurs</a></strong></li>
      <li><strong><a href="devel-manuals">Manuels pour les développeurs</a></strong></li>
      <li><strong><a href="misc-manuals">Autres manuels</a></strong></li>
      <li><strong><a href="#other">Manuels problématiques</a></strong></li>
    </ul>
 </div>

  <div class="item col50 lastcol">

    <h3><a href="docpolicy">Politique de documentation</a></h3>
    <ul>
      <li>Licences de manuel compatibles avec les DFSG.</li>
      <li>Nous utilisons Docbook XML pour nos documents.</li>
      <li>Les sources devraient être sur
      <a href="https://salsa.debian.org/ddp-team">https://salsa.debian.org/ddp-team</a>.</li>
      <li><tt>www.debian.org/doc/&lt;nom_de_manuel&gt;</tt> sera l’URL
       officielle.</li>
      <li>Chaque document devrait être entretenu activement.</li>
      <li>Veuillez demander sur
      <a href="https://lists.debian.org/debian-doc/">debian-doc</a> (en anglais) si vous
       désirez écrire un nouveau document.</li>
    </ul>


    <h3>Accès à Git</h3>
    <ul>
      <li><a href="vcs">Comment accéder</a> aux dépôts Git du DDP.</li>
    </ul>

  </div>

</div>

<hr />

<h2><a name="other">Manuels problématiques</a></h2>

<p>En complément à ces manuels, nous conservons les manuels suivants qui,
d'une façon ou d'une autre, restent problématiques&nbsp;: nous ne pouvons donc
pas les recommander à tous les utilisateurs, mais nous les laissons tout de
même disponibles, sans aucune garantie.</p>

<ul>
  <li>Le <a href="obsolete#tutorial">Cours Debian</a> est obsolète.</li>
  <li>Le <a href="obsolete#guide">Guide Debian</a> est obsolète.</li>
  <li>Le <a href="obsolete#userref">Manuel de référence de l'utilisateur
      Debian</a> est au point mort et assez incomplet.</li>
  <li>Le <a href="obsolete#system">Manuel de l'administrateur système
      Debian</a> est au point mort et quasiment vide.</li>
  <li>Le <a href="obsolete#network">Manuel de l'administrateur réseau
      Debian</a> est au point mort et incomplet.</li>
  <li>Le manuel <a href="obsolete#swprod">Comment les créateurs de
      logiciels peuvent distribuer leurs produits directement au format .deb</a>
      est au point mort et obsolète.</li>
  <li>Le <a href="obsolete#packman">Manuel de programmation de dpkg</a>
      est en partie intégré dans la <a href="devel-manuals#policy">Charte
      Debian</a>, le reste servira à un futur manuel de référence de dpkg.</li>
  <li>Le manuel <a href="obsolete#makeadeb">Introduction&nbsp;: créer un
      paquet Debian</a> est rendu obsolète par le <a
      href="devel-manuals#maint-guide">Guide des nouveaux responsables
      Debian</a>.</li>
  <li>Le <a href="obsolete#programmers">Manuel des programmeurs Debian</a> est
      rendu obsolète par le
      <a href="devel-manuals#maint-guide">Guide des nouveaux responsables Debian</a>
      et par le
      <a href="devel-manuals#debmake-doc">Guide des responsables de paquet
      Debian</a>.</li>
  <li>Le <a href="obsolete#repo">Guide pratique des référentiels Debian</a> est
      rendu obsolète par l'introduction de la version sécurisée d'APT.</li>
  <li>L’<a href="obsolete#i18n">Introduction à i18n</a> est au point mort.</li>
  <li>Le <a href="obsolete#sgml-howto">SGML/XML HOWTO</a> de Debian est au point mort
      et obsolète.</li>
  <li>Le <a href="obsolete#markup">Manuel DebianDoc-SGML Markup</a> est au point mort.
       DebianDoc est sur le point d’être supprimé.</li>
  <li>Le manuel <a href="obsolete#l10n-french">Utiliser et configurer Debian
      pour le français</a> est au point mort et obsolète.</li>
</ul>
