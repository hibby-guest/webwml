#use wml::debian::translation-check translation="503631eef276fe201ab3324c58a5e7f08bdd0509" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Bullseye Alpha 3</define-tag>
<define-tag release_date>2020-12-06</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la troisième version candidate pour Debian 11 <q>Bullseye</q>.
</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>apt-setup :
    <ul>
      <li>retrait de la mention du dépôt volatile du fichier sources.list
        généré (<a href="https://bugs.debian.org/954460">nº 954460</a>).</li>
    </ul>
  </li>
  <li>base-installer :
    <ul>
      <li>amélioration de l'architecture de test, ajout de la prise en charge
        des versions Linux 5.x.</li>
    </ul>
  </li>
  <li>brltty :
    <ul>
      <li>amélioration de la détection du matériel et de la prise en charge
        des pilotes.</li>
    </ul>
  </li>
  <li>cdebconf :
    <ul>
      <li>rapport de progression de l'interface texte plus précis : depuis le
        tout début et également à partir de la réponse à une question.</li>
    </ul>
  </li>
  <li>choose-mirror :
    <ul>
      <li>mise à jour de Mirrors.masterlist.</li>
    </ul>
  </li>
  <li>console-setup :
    <ul>
      <li>amélioration de la prise en charge des caractères de tracé de cadres
        (<a href="https://bugs.debian.org/965029">nº 965029</a>).</li>
      <li>synchronisation des fontes Terminus avec le paquet xfonts-terminus ;</li>
      <li>correction de la présentation du lituanien (<a href="https://bugs.debian.org/951387">nº 951387</a>).</li>
    </ul>
  </li>
  <li>debian-cd :
    <ul>
      <li>inclusion des udeb de Linux seulement pour la dernière ABI, rendant
        les petites images d'installation plus utiles.</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>passage de l'ABI du noyau Linux à la version 5.9.0-4 ;</li>
      <li>abandon des améliorations de fontconfig introduites dans
        l'installateur Debian Buster Alpha 1 (voir : <a href="https://bugs.debian.org/873462">nº 873462</a>) ;</li>
      <li>installation de kmod-udeb à la place de libkmod2-udeb ;</li>
      <li>imitation de la gestion de libgcc1 pour libgcc-s1 ;</li>
      <li>nettoyage de la liste de paquets factices ;</li>
      <li>remplacement de la passe de réduction de la bibliothèque mklibs par
        une bidouille, en copiant libgcc_s.so.[124] à partir du système de
        fichiers hôte pour l'instant ;</li>
      <li>ajout explicite des dépendances de construction à fdisk sur arm64,
        amd64 et i386 maintenant qu'util-linux ne dépend plus de lui ;</li>
      <li>ajout de grub2 à built-using (<a href="https://bugs.debian.org/968998">nº 968998</a>) ;</li>
      <li>correction de FTBFS avec fakeroot en adaptant la vérification de
        /dev/console (voir <a href="https://bugs.debian.org/940056">nº 940056</a>).</li>
    </ul>
  </li>
  <li>debian-installer-utils :
    <ul>
      <li>adaptation de l'utilisation par fetch-url des descripteurs de fichier
        pour les versions récentes d'udev (<a href="https://bugs.debian.org/967546">nº 967546</a>).</li>
    </ul>
  </li>
  <li>debootstrap :
    <ul>
      <li>installation de apt-transport-https seulement sur Stretch et les
        versions précédentes, la prise en charge de HTTPS a été incluse dans le
        paquet principal d'apt pour Buster (<a href="https://bugs.debian.org/920255">nº 920255</a>, <a href="https://bugs.debian.org/879755">nº 879755</a>).</li>
    </ul>
  </li>
  <li>finish-install :
    <ul>
      <li>retrait complet de la prise en charge de upstart (<a href="https://bugs.debian.org/923845">nº 923845</a>).</li>
    </ul>
  </li>
  <li>fonts-noto :
    <ul>
      <li>correction de la prise en charge du singhalais dans l'installateur
        (<a href="https://bugs.debian.org/954948">nº 954948</a>).</li>
    </ul>
  </li>
  <li>grub-installer :
    <ul>
      <li>ajustement des écrans pour les adapter aux systèmes UEFI et aux
        nouveaux types de support de stockage du système (<a href="https://bugs.debian.org/954718">nº 954718</a>).</li>
    </ul>
  </li>
  <li>kmod :
    <ul>
      <li>séparation de kmod-udeb de libkmod2-udeb et fourniture maintenant des
        bibliothèques dans libkmod2-udeb (<a href="https://bugs.debian.org/953952">nº 953952</a>).</li>
    </ul>
  </li>
  <li>locale-chooser :
    <ul>
      <li>activation de nouvelles langues : kabyle, occitan.</li>
    </ul>
  </li>
  <li>partman-auto :
    <ul>
      <li>passage de la taille de /boot dans la plupart des recettes d'entre
        128 et 256 Mo à entre 512 et 768 Mo (<a href="https://bugs.debian.org/893886">nº 893886</a>, <a href="https://bugs.debian.org/951709">nº 951709</a>) ;</li>
      <li>importation de la prise en charge de partman-auto/cap-ram à partir
        d'Ubuntu, pour permettre de plafonner la taille de la RAM comme utilisé
        pour les calculs de partition swap (<a href="https://bugs.debian.org/949651">nº 949651</a>, <a href="https://bugs.debian.org/950344">nº 950344</a>).
        Cela permet de limiter la taille minimale des partitions swap à 1*CAP,
        et leur taille maximale à un maximum de 2 ou 3*CAP selon
        l'architecture. Par défaut elle est fixée à 1024 Mo, donc cela limite
        les partitions swap entre 1 et 3 Go.</li>
    </ul>
  </li>
  <li>partman-efi :
    <ul>
      <li>remontage de /cdrom en lecture et écriture s'il se trouve être aussi
        utilisé comme /boot/efi (<a href="https://bugs.debian.org/967918">nº 967918</a>) ;</li>
      <li>suppression de l'utilisation du module efivars, et plus de recherche
        de /proc/efi. efivarfs est l'interface actuelle, et /proc/efi n'est
        plus là depuis longtemps.</li>
    </ul>
  </li>
  <li>partman-partitioning :
    <ul>
      <li>inclusion de ntfs-3g-udeb sur arm64.</li>
    </ul>
  </li>
  <li>partman-target :
    <ul>
      <li>ajout d'une indication au nouveau fstab sur l'utilisation de
        <code>systemctl daemon-reload</code> après la modification de
        /etc/fstab (<a href="https://bugs.debian.org/963573">nº 963573</a>).</li>
    </ul>
  </li>
  <li>systemd :
    <ul>
      <li>installation de 60-block.rules dans udev-udeb (<a href="https://bugs.debian.org/958397">nº 958397</a>).
        Les règles de périphérique bloc ont été séparées de
        60-persistent-storage.rules dans la version 220. Cela corrige un
        bogue de longue date où les UUID ne devraient pas être utilisés pour
        les systèmes de fichiers lors de l'installation initiale.</li>
    </ul>
  </li>
  <li>util-linux :
    <ul>
      <li>remplacement d'eject-udeb (<a href="https://bugs.debian.org/737658">nº 737658</a>).</li>
    </ul>
  </li>
  <li>win32-loader :
    <ul>
      <li>introduction du gestionnaire de démarrage d'UEFI et de la prise en
        charge de Secure Boot (<a href="https://bugs.debian.org/918863">nº 918863</a>).</li>
    </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>debian-cd :
    <ul>
      <li>activation de l'installateur graphique pour arm64 ;</li>
      <li>exclusion des udeb lilo-installer et elilo-installer pour toutes les
        architectures ;</li>
      <li>arrêt de la fabrication des images de CD uniques pour XFCE ;</li>
      <li>arrêt de la fabrication des images ISO des DVD 2 et 3 pour
         amd64/i386 (elles sont encore disponibles avec jigdo).</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>mise à jour de l'image Firefly-RK3288 pour la nouvelle version d'u-boot ;</li>
      <li>[arm64] ajout de la prise en charge de firefly-rk3399, pinebook-pro-rk3399,
        rockpro64-rk3399, rock64-rk3328 et de rock-pi-4-rk3399 aux images u-boot
        et aux images sur carte SD d'amorçage réseau ;</li>
      <li>[arm64] toutes les images sur carte SD d'amorçage réseau démarrent à
        la position 32768, pour une compatibilité avec les plateformes
        rockchip ;</li>
      <li>ajout de prise en charge de l'ordinateur portable OLPC XO-1.75
        (<a href="https://bugs.debian.org/949306">nº 949306</a>) ;</li>
      <li>activation de la construction de GTK pour arm64 ;</li>
      <li>ajout de la prise en charge de NanoPi NEO Air (<a href="https://bugs.debian.org/928863">nº 928863</a>) ;</li>
      <li>ajout de wireless-regdb-udeb aux constructions de Linux qui incluent
        nic-wireless-modules ;</li>
      <li>efi-image : amélioration du calcul de taille pour réduire l'espace
        perdu ;</li>
      <li>efi-image : inclusion des fichiers DTB dans l'ESP pour les systèmes
        armhf et arm64. Cela pourrait faire mieux fonctionner les systèmes 
        basés sur U-Boot lors d'un démarrage avec UEFI.</li>
    </ul>
  </li>
  <li>flash-kernel :
    <ul>
      <li>ajout du NanoPi NEO Plus2 de FriendlyARM (<a href="https://bugs.debian.org/955374">nº 955374</a>) ;</li>
      <li>ajout de Pinebook (<a href="https://bugs.debian.org/930098">nº 930098</a>) ;</li>
      <li>ajout de Pinebook Pro ;</li>
      <li>ajout des A64-Olinuxino et A64-Olinuxino-eMMC d'Olimex
        (<a href="https://bugs.debian.org/931195">nº 931195</a>) ;</li>
      <li>ajout des LX2160A Honeycomb et Clearfog CX de SolidRun
        (<a href="https://bugs.debian.org/958023">nº 958023</a>) ;</li>
      <li>ajout des variantes Cubox-i Solo/DualLite de SolidRun (<a href="https://bugs.debian.org/939261">nº 939261</a>) ;</li>
      <li>ajout de Turris MOX (<a href="https://bugs.debian.org/961303">nº 961303</a>).</li>
    </ul>
  </li>
  <li>linux :
    <ul>
      <li>passage de tous les modules de compression dans le paquet udeb
        kernel-image ; abandon du paquet udeb compress-modules ;</li>
      <li>input-modules udeb dépend de crc-modules ;</li>
      <li>[arm64] ajout d'i2c_mv64xxx au paquet udeb i2c-modules ;</li>
      <li>[arm64] ajout de drivers/pinctrl au paquet udeb kernel-image ;</li>
      <li>[arm64] ajout d'analogix-anx6345, pwm-sun4i, sun4i-drm et
        sun8i-mixer au paquet udeb fb-modules ;</li>
      <li>[arm64] ajout de pwm-sun4i au paquet udeb fb-modules ;</li>
      <li>[arm64] ajout d'armada_37xx_wdt au paquet udeb kernel-image
        (<a href="https://bugs.debian.org/961086">nº 961086</a>) ;</li>
      <li>[mips*] abandon du paquet udeb hfs-modules ;</li>
      <li>[x86] ajout du paquet udeb crc32_pclmul à crc-modules  ;</li>
      <li>ajout de crc32_generic au paquet udeb crc-modules ;</li>
      <li>inversion de l'ordre des udeb cdrom-core et isofs/udf : le second
        requiert maintenant le premier ;</li>
      <li>abandon du paquet udeb zlib-modules (zlib_deflate est désormais toujours
        intégré) ;</li>
      <li>ajout du paquet udeb f2fs-modules.</li>
    </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>78 langues sont prises en charge dans cette version.</li>
  <li>Nouvelles langues : kabyle, occitan.
  <li>La traduction est complète pour seize de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
