#use wml::debian::translation-check translation="a519393250458ed72be6cc6df911105563ae120c"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto dos problemas de seguridad en LibreOffice:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9848">CVE-2019-9848</a>

    <p>Nils Emmerich descubrió que documentos maliciosos podrían ejecutar
    código Python arbitrario a través de LibreLogo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9849">CVE-2019-9849</a>

    <p>Matei Badanoiu descubrió que el modo «stealth» no se aplicaba a
    los gráficos de los topos usados para introducir los elementos de las enumeraciones o listas.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 1:5.2.7-1+deb9u9.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 1:6.1.5-3+deb10u2.</p>

<p>Le recomendamos que actualice los paquetes de libreoffice.</p>

<p>Para información detallada sobre el estado de seguridad de libreoffice, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/libreoffice">\
https://security-tracker.debian.org/tracker/libreoffice</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4483.data"
