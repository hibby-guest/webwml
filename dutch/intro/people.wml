#use wml::debian::template title="De mensen: wie we zijn, wat we doen"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="bbb1b67054b9061c9420f1c5826b2fa85f05c738"

# translators: some text is taken from /intro/about.wml

<h2>Ontwikkelaars en medewerkers</h2>
<p>Debian wordt gemaakt door bijna duizend actieve
ontwikkelaars, verspreid
<a href="$(DEVEL)/developers.loc">over de hele wereld</a>,
die als vrijwilliger meewerken in hun vrije tijd.
Slechts weinig ontwikkelaars hebben elkaar persoonlijk ontmoet.
Communicatie verloopt voornamelijk via e-mail (mailinglijsten op
lists.debian.org) en IRC (#debian kanaal op irc.debian.org).
</p>

<p>De volledige lijst van officiële leden van Debian is te vinden op
<a href="https://nm.debian.org/members">nm.debian.org</a>, waar
het lidmaatschapsbeheer gebeurt. Een ruimere lijst van mensen die
bijdragen aan Debian, vindt men op
<a href="https://contributors.debian.org">contributors.debian.org</a>.</p>

<p>Het Debian Project heeft een zorgvuldig uitgebouwde
<a href="organization">organisatiestructuur</a>.
Voor meer informatie over hoe Debian er van binnenuit uitziet, kunt u eens
rondkijken in <a href="$(DEVEL)/">de hoek voor ontwikkelaars</a>.</p>

<h3><a name="history">Hoe is het allemaal begonnen?</a></h3>

<p>Het Debian project werd in augustus 1993 gestart door Ian Murdock,
als een nieuwe distributie die openlijk ontwikkeld zou worden, in de geest
van Linux en GNU. Het was de bedoeling om Debian voorzichtig en
zorgvuldig op te bouwen en het even goed te onderhouden en
ondersteunen. Het begon als een kleine, hechte groep ontwikkelaars van
Vrije Software en groeide geleidelijk uit tot een grote, goed
georganiseerde gemeenschap van ontwikkelaars en gebruikers. Zie
<a href="$(DOC)/manuals/project-history/">de gedetailleerde geschiedenis</a>.

<p>Omdat veel mensen er naar vragen: Debian wordt uitgesproken als
/&#712;de.bi.&#601;n/. De naam is afgeleid van de naam van de initiatiefnemer
van Debian, Ian Murdock, en van die van zijn vrouw, Debra.

<h2>Individuen en organisaties die Debian steunen</h2>

<p>Veel andere individuen en organisaties maken deel uit van de Debian-gemeenschap:
<ul>
  <li><a href="https://db.debian.org/machines.cgi">Hosting- en hardwaresponsors</a></li>
  <li><a href="../mirror/sponsors">Spiegelserversponsors</a></li>
  <li><a href="../partners/">Ontwikkelings- en servicepartners</a></li>
  <li><a href="../consultants">Consultants</a></li>
  <li><a href="../CD/vendors">Verkopers van Debian-installatiemedia</a></li>
  <li><a href="../distrib/pre-installed">Computerverkopers die Debian vooraf installeren</a></li>
  <li><a href="../events/merchandise">Verkopers van koopwaar</a></li>
</ul>

<h2><a name="users">Wie gebruikt Debian?</a></h2>

<p>Hoewel er geen precieze statistieken beschikbaar zijn (omdat Debian
niet vereist dat gebruikers zich registreren), is er duidelijk bewijs dat
een grote verscheidenheid aan organisaties, groot en klein, Debian gebruikt,
net als vele duizenden individuen. Op de pagina <a href="../users">Wie gebruikt
Debian?</a> kunt u een overzicht vinden van bekende organisaties die Debian
gebruiken en die ons een korte beschrijving hebben gegeven van hoe en waarom
ze Debian gebruiken.
