#use wml::debian::ddp title="Debian Documentatie Project"
#use wml::debian::translation-check translation="60eea6916091f1a3a9240f472ebea1904c32d0bd"

# Translator: $Author$
# Last update: $Date$

<p>Het Debian Documentatie Project heeft als doel alle inspanningen om meer
en betere documentatie voor Debian te schrijven, te coördineren en samen te
bundelen.</p>

  <h2>DDP-werk</h2>
<div class="line">
  <div class="item col50">

    <h3>Handleidingen</h3>
    <ul>
	  <li><strong><a href="user-manuals">Handleidingen voor
	      gebruikers</a></strong></li>
	  <li><strong><a href="devel-manuals">Handleidingen voor
	      ontwikkelaars</a></strong></li>
      <li><strong><a href="misc-manuals">Diverse handleidingen</a></strong></li>
      <li><strong><a href="#other">Problematische
	      handleidingen</a></strong></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h3>Documentatiebeleid</h3>
    <ul>
      <li>De licenties van de handleidingen moeten voldoen aan de DFSG.</li>
	  <li>We gebruiken Docbook XML voor onze documenten.</li>
      <li>De broncode moet te vinden zijn op <a href="https://salsa.debian.org/ddp-team">https://salsa.debian.org/ddp-team</a>.</li>
      <li><tt>www.debian.org/doc/&lt;naam-van-de-handleiding&gt;</tt> is de officiële URL.</li>
      <li>Elk document moet actief onderhouden worden.</li>
      <li>Als u graag een nieuw document wilt schrijven, overleg dit dan eerst op <a href="https://lists.debian.org/debian-doc/">debian-doc</a>.</li>
    </ul>

    <h3>Git-toegang</h3>
    <ul>
	  <li><a href="vcs">Hoe men toegang verkrijgt tot</a> de git-opslagplaatsen van
	      het DDP.</li>
    </ul>

  </div>

</div>


<hr />

<h2><a name="other">Problematische handleidingen</a></h2>

<p>In aanvulling op de handleidingen die op de gebruikelijke manier bekend
gemaakt worden, onderhouden we ook de volgende handleidingen. Deze zijn
op de een of andere manier problematisch, dus we raden deze niet aan aan
alle gebruikers. Gebruik op eigen risico.</p>

<ul>
  <li><a href="obsolete#tutorial">Debian Tutorial</a>, verouderd</li>
  <li><a href="obsolete#guide">Debian Guide</a>, verouderd</li>
  <li><a href="obsolete#userref">Debian User Reference Manual</a>,
      wordt niet meer bijgewerkt, behoorlijk onvolledig</li>
  <li><a href="obsolete#system">Debian System Administrator's
      Manual</a>, wordt niet meer bijgewerkt, bijna leeg</li>
  <li><a href="obsolete#network">Debian Network Administrator's
      Manual</a>, wordt niet meer bijgewerkt, onvolledig</li>
  <li><a href="obsolete#swprod">How Software Producers can distribute
      their products directly in .deb format</a>, wordt niet meer bijgewerkt,
      verouderd</li>
  <li><a href="obsolete#packman">Debian Packaging Manual</a>, gedeeltelijk
      opgenomen in het <a href="devel-manuals#policy">Debian Beleidshandboek</a>,
      de rest zal worden opgenomen in een dpkg referentiehandleiding
      die nog moet worden geschreven.</li>
  <li><a href="obsolete#makeadeb">Introduction: Making a Debian
      Package</a> achterhaald door de
      <a href="devel-manuals#maint-guide">Debian New Maintainers’ Guide</a></li>
  <li><a href="obsolete#programmers">Debian Programmers’ Manual</a>,
      achterhaald door de
      <a href="devel-manuals#maint-guide">Debian New Maintainers’ Guide</a>
      en de
      <a href="devel-manuals#debmake-doc">Guide for Debian Maintainers</a></li>
  <li><a href="obsolete#repo">Debian Repository HOWTO</a>, verouderd na de
      introductie van veilig APT</li>
  <li><a href="obsolete#i18n">Introduction to i18n</a>, wordt niet meer bijgewerkt</li>
  <li><a href="obsolete#sgml-howto">Debian SGML/XML HOWTO</a>, wordt niet meer bijgewerkt, verouderd</li>
  <li><a href="obsolete#markup">Debiandoc-SGML Markup Manual</a>, wordt niet meer bijgewerkt; DebianDoc staat op het punt te worden verwijderd</li>
</ul>
