#use wml::debian::template title="Git gebruiken om de broncode van de website te manipuleren"
#use wml::debian::translation-check translation="5ec84105c40bf62785fc1159fcf33ed470a2fbc7"

<h2>Inleiding</h2>

<p>Git is een
<a href="https://en.wikipedia.org/wiki/Version_control">versiebeheersysteem</a>
dat helpt om meerdere mensen tegelijkertijd aan hetzelfde materiaal te laten
werken. Elke gebruiker kan een lokale kopie van een hoofddepot hebben.
Gebruikers kunnen dan de lokale kopie naar wens aanpassen en wanneer het
gewijzigde materiaal klaar is, de wijzigingen vastleggen en terug doorsturen
naar het hoofddepot.</p>

<p>Git zal u niet direct een vastlegging laten doorsturen als het
hoofddepot op dezelfde tak nieuwere vastleggingen (wijzigingen) bevat
dan uw lokale kopie. In een dergelijk geval waarbij er een conflict is, moet u
eerst deze wijzigingen ophalen en uw lokale kopie updaten en dan met het
commando <code>rebase</code> naar behoefte uw nieuwe wijzigingen bovenop de
laatste vastlegging toevoegen.
</p>

<h3><a name="write-access">Schrijftoegang tot het Git-depot</a></h3>

<p>
De volledige broncode van de Debian-website wordt beheerd onder Git. Ze bevindt
zich op <url https://salsa.debian.org/webmaster-team/webwml/>. Standaard mogen
gasten geen vastleggingen doorsturen naar het broncodedepot. U zult een
bepaalde vorm van toestemming nodig hebben om schrijftoegang te krijgen tot het
depot.
</p>

<h4><a name="write-access-unlimited">Onbeperkte schrijftoegang</a></h4>
<p>
Als u onbeperkte schrijftoegang tot het depot nodig heeft (bijv. wanneer u op
het punt staat om frequente bijdragen te gaan leveren), overweeg dan om
schrijftoegang aan te vragen via de
<url https://salsa.debian.org/webmaster-team/webwml/> webinterface van het
Salsa-platform van Debian nadat u erop ingelogd bent.
</p>

<p>
Als u nieuw bent bij de ontwikkeling van de website van Debian en geen eerdere
ervaring hebt, zend dan ook een e-mail naar
<a href="mailto:debian-www@lists.debian.org"> debian-www@lists.debian.org</a>
waarin u zichzelf voorstelt, voor u om onbeperkte schrijftoegang vraagt. Geef
in uw voorstelling nuttige informatie, zoals in welke taal of rond welk deel
van de website u van plan bent te werken en wie voor u kan garant staan.
</p>

<h4><a name="write-access-via-merge-request">Schrijven in het depot via een Merge Request</a></h4>
<p>
Als u niet van plan bent om onbeperkte schrijftoegang tot het depot te
verkrijgen of dit niet kunt, kunt u altijd een zogenaamd Merge
Request (een verzoek om uw bijdrage in te voegen) indienen waarna andere
ontwikkelaars uw werk kunnen nakijken en het aanvaarden. Dien een Merge Request
in via de standaardprocedure zoals voorzien door het Salsa GitLab platform via
zijn webinterface.
</p>

<p>
Niet alle website-ontwikkelaars zien toe op de Merge Requests en dus kan het
zijn dat ze niet altijd tijdig worden verwerkt. Als u niet zeker weet of men uw
bijdrage zou accepteren, stuur dan een e-mail naar de mailinglijst
<a href="https://lists.debian.org/debian-www/">debian-www</a> en vraag om een
beoordeling.
</p>

<h2><a name="work-on-repository">Aan het depot werken</a></h2>

<h3><a name="get-local-repo-copy">Een lokale kopie van het depot ophalen</a></h3>

<p>Om aan het depot te werken moet u eerst git installeren. Daarna moet u
op uw computer uw gebruikers- en e-mailgegevens configureren (raadpleeg
algemene informatie over git om te weten hoe u dit moet doen). Vervolgens kunt
u het depot klonen (een lokale kopie ervan maken, met andere woorden) op
een van beide manieren.</p>

<p>De aanbevolen manier om aan webwml te werken, is eerst een account te
registreren op salsa.debian.org en SSH-toegang met git in te schakelen door een
openbare SSH-sleutel te uploaden naar uw salsa-account. Raadpleeg de <a
href="https://salsa.debian.org/help/ssh/README.md">salsa hulppagina's</a> voor
meer informatie over hoe u dit moet doen. Daarna kunt u het webwml-depot klonen
met het volgende commando:</p>

<pre>
   git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>

<p>Indien u geen account op salsa heeft, is een andere mogelijkheid om het
depot te klonen met het HTTPS-protocol:</p>

<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>

<p>Dit geeft u lokaal eenzelfde kopie van het depot, maar op deze manier zult u niet in staat zijn om wijzigingen rechtstreeks naar het depot door te sturen.</p>

<p>Om het volledige webwml-depot te klonen moet u ongeveer 500MB data
downloaden. Voor personen met een trage of onstabiele internetverbinding kan
dit moeilijk zijn. U kunt dan eerst proberen ondiep te klonen met een minimale
diepte voor een kleinere initiële download:</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>Nadat u een bruikbare (ondiepe) depotkopie verkregen heeft, kunt u deze
lokale ondiepe kopie verder uitdiepen en uiteindelijk omzetten naar een
volledige lokale kopie van het depot: </p>

<pre>
  git fetch --deepen=1000 # de depotkopie verder uitdiepen
                            met 1.000 extra vastleggingen
  git fetch --unshallow   # alle ontbrekende vastleggingen ophalen en de
                            depotkopie omzetten naar een volledige kopie
</pre>

<h4><a name="partial-content-checkout">Een kopie maken van een gedeelte van de inhoud</a></h4>

<p>Op de volgende manier kunt u een checkout uitvoeren om slechts een
deelverzameling van de pagina's binnen te halen:</p>

<pre>
   $ git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git
   $ cd webwml
   $ git config core.sparseCheckout true
   In webwml: maak het bestand .git/info/sparse-checkout aan met een inhoud
   zoals de volgende (indien u enkel de basisbestanden en de Engelse, Franse en
   Nederlandse vertaling wilt):
      /*
      !/[a-z]*/
      /english/
      /french/
      /dutch/
   Geef daarna het volgende commando:
   $ git checkout --
</pre>

<h3><a name="submit-changes">Lokale wijzigingen indienen</a></h3>

<h4><a name="keep-local-repo-up-to-date">Uw lokale depotkopie up-to-date houden</a></h4>

<p>Om de paar dagen (en zeker voordat u met bewerken begint!) moet u het
volgende commando uitvoeren: </p>

<pre>
   git pull
</pre>

<p>om alle gewijzigde bestanden op te halen uit het depot.</p>

<p>
Het wordt sterk aanbevolen om uw lokale git-werkmap schoon te houden voordat u
het commando "git pull" uitvoert en na het bewerken van bestanden. Als u op de
huidige tak nog niet-vastgelegde wijzigingen heeft of lokale vastleggingen die
niet aanwezig zijn in het externe depot, zal het uitvoeren van het
commando "git pull" automatisch samenvoegingsvastleggingen creëren of zelfs
mislukken vanwege conflicten. Denk erover om uw onvoltooide werk in een andere
tak te houden of commando's te gebruiken zoals "git stash".
</p>

<p>Opmerking: git is een gedistribueerd (niet een gecentraliseerd)
versiebeheersysteem. Dit betekent dat wanneer u wijzigingen vastlegt, deze
enkel in uw lokale depotkopie opgeslagen worden. Om deze met andere te delen,
zult u ook uw wijzigingen naar het centrale depot op salsa moeten doorsturen
met het commando push.</p>

<h4><a name="example-edit-english-file">Een voorbeeld van het bewerken van de Engelse bestanden</a></h4>

<p>
Hier vindt u een voorbeeld van het bewerken van de Engelse bestanden in het
broncodedepot van de website. Na het verkrijgen van een lokale kopie van het
depot met "git clone" en voordat u met het bewerken begint, moet u het volgende
commando uitvoeren:
</p>

<pre>
   $ git pull
</pre>

<p>Breng nu wijzigingen aan in bestanden. Als u klaar bent, legt u uw
wijzigingen vast in uw lokaal depot met:</p>

<pre>
   $ git add pad/naar/bestand(en)
   $ git commit -m "Uw vastleggingsbericht"
</pre>

<p>Als u onbeperkte schrijftoegang heeft tot het externe webwml-depot, kunt u
uw wijzigingen nu rechtstreeks doorsturen naar het Salsa-depot:</p>

<pre>
   $ git push
</pre>

<p>Als u geen directe schrijftoegang heeft tot het webwml-depot, denk er dan
aan om uw wijzigingen in te dienen met behulp van de functie Merge Request die
ter beschikking gesteld wordt via het GitLab-platform van Salsa
of om andere ontwikkelaars om hulp te vragen.
</p>

<p>Dit is een heel eenvoudige samenvatting van hoe git moet worden gebruikt om
de broncode van de Debian-website te manipuleren. Lees de documentatie van git
voor meer informatie over git.</p>

### FIXME: Is the following still true? holgerw
### FIXME: Seems not true anymore. Needs fixup. -- byang
<h4><a name="closing-debian-bug-in-git-commits">Debian bugs sluiten in git-vastleggingen</a></h4>

<p>
Indien u in uw mededeling bij de git-vastlegging vermeldt
<code>Closes: #</code><var>nnnnnn</var>, dan zal
bugnummer <code>#</code><var>nnnnnn</var> automatisch gesloten worden wanneer u
met het commando push uw wijzigingen doorstuurt. De exacte vorm ervan is
dezelfde als <a href="$(DOC)/debian-policy/ch-source.html#id24">in het Debian
beleidshandboek</a>.</p>

<h4><a name="links-using-http-https">Linken met HTTP/HTTPS</a></h4>

<p>Veel Debian-websites ondersteunen SSL/TLS. Gebruik daarom waar mogelijk en
raadzaam HTTPS-links. <strong>Echter</strong>, sommige
Debian/DebConf/SPI/enz websites hebben ofwel geen ondersteuning voor HTTPS of
maken enkel gebruik van het CA van SPI (en geen SSL-CA waarin alle browsers
vertrouwen stellen). Om te vermijden dat niet-Debian-gebruikers geconfronteerd
worden met foutmeldingen, moet u dergelijke sites niet met HTTPS linken.</p>

<p>Het git-depot weigert vastleggingen die gewone HTTP-links bevatten voor
Debian-websites die HTTPS ondersteunen of die HTTPS-links bevatten voor de
websites van Debian/DebConf/SPI waarvan bekend is dat ze geen HTTPS
ondersteunen of certificaten gebruiken die alleen zijn ondertekend door SPI.</p>

<h3><a name="translation-work">Aan vertalingen werken</a></h3>

<p>Vertalingen moeten altijd worden bijgehouden om up-to-date te zijn met het
overeenkomstige Engelse bestand. De kopregel "translation-check" in vertaalde
bestanden wordt gebruikt om bij te houden op welke versie van het Engelse
bestand de huidige vertaling gebaseerd is. Indien u een vertaald bestand
wijzigt, moet u de kopregel "translation-check" bijwerken, zodat die
overeenkomt met de vastleggingsfrommel van de overeenkomstige aanpassing van
het Engelse bestand. U kunt deze frommel terugvinden met</p>

<pre>
$ git log pad/naar/het/Engelse/bestand
</pre>

<p>Indien u een nieuwe vertaling gaat maken van een bestand, moet u het script
<q>copypage.pl</q> gebruiken. Dit zal een sjabloon aanmaken voor uw taal, met
inbegrip van een correcte kopregel "translation-check".</p>

<h4><a name="translation-smart-change">Vertalingen aanpassen met
smart_change.pl</a></h4>

<p><code>smart_change.pl</code> is a script dat ontworpen is om het gezamenlijk
updaten van originele bestanden en hun vertalingen makkelijker te maken. U kunt
het op twee manieren gebruiken, afhankelijk van welke wijzigingen u aanbrengt.
</p>

<p>Wanneer u <code>smart_change</code> gebruikt om gewoon de kopregels
"translation-check" aan te passen in het geval u handmatig aan de bestanden
werkt:</p>

<ol>
  <li>Breng de wijzigingen aan in het/de origine(e)l(e) bestand(en), en leg
    deze vast</li>
  <li>Update de vertalingen</li>
  <li>Voer het commando smart_change.pl uit - dit zal de aanpassingen oppikken
     en in de vertaalde bestanden de kopregel updaten</li>
  <li>Controleer de wijzigingen (bijv. met "git diff")</li>
  <li>Leg de gewijzigde vertalingen vast</li>
</ol>

<p>En anders, wanneer u smart_change gebruikt met een reguliere expressie om in
één keer wijzigingen in meerdere bestanden aan te brengen:</p>

<ol>
  <li>Geef het commando <code>smart_change.pl -s s/FOO/BAR/ origbestand1
    origbestand2 ...</code></li>
  <li>Controleer de wijzigingen (bijv. met <code>git diff</code>)
  <li>Leg het/de origine(e)l(e) bestand(en) vast</li>
  <li>Geef het commando <code>smart_change.pl origbestand1 origbestand2</code>
    (d.w.z. ditmaal <strong>zonder de reguliere expressie</strong>; dit zal nu
    enkel de kopregels in de vertaalde bestanden updaten</li>
  <li>Leg tenslotte de gewijzigde vertalingen vast</li>
</ol>

<p>Dit heeft wat meer voeten in de aarde dan de vorige werkwijze (er zijn
twee vastleggingen nodig), maar dit is onvermijdelijk vanwege de manier waarop
git met vastleggingshashes werkt.</p>

<h2><a name="notifications">Meldingen ontvangen</a></h2>

<h3><a name="commit-notifications">Vastleggingsmeldingen ontvangen</a></h3>

<p>We hebben het webwml-project in Salsa zo geconfigureerd dat vastleggingen
worden getoond in het IRC-kanaal #debian-www.</p>

<p>Als u via e-mail meldingen wilt ontvangen wanneer er vastleggingen gebeuren
in het webwml-depot, abonneer u dan op het pseudopakket <q>www.debian.org</q>
via tracker.debian.org en activeer daar het trefwood <q>vcs</q> door deze
stappen te volgen (slechts één keer):</p>

<ul>
  <li>Open een webbrowser en ga naar <url https://tracker.debian.org/pkg/www.debian.org></li>
  <li>Teken in op het pseudopakket <q>www.debian.org</q>. (U kunt authenticeren
      via SSO of een e-mail en wachtwoord registreren, indien u niet reeds
      gebruik maakte van tracker.debian.org voor andere doeleinden).</li>
  <li>Ga naar <url https://tracker.debian.org/accounts/subscriptions/> en dan
      naar <q>modify keywords</q>, vink <q>vcs</q> aan (als het nog niet
      aangevinkt is) en sla op.</li>
  <li>Vanaf dan zult u een e-mail ontvangen wanneer iemand een vastlegging in
      het webwml-depot doet. We zullen binnenkort de andere depots van het
      webmasterteam toevoegen.</li>
</ul>

<h3><a name="merge-request-notifications">Een melding van een Merge Request
ontvangen</a></h3>

<p>
Als u een melding via e-mail wilt ontvangen wanneer er een nieuw Merge Request
ingediend werd op de webinterface van het webwml-depot op het GitLab-platform
van Salsa, kunt u uw instellingen voor meldingen over het webwml-depot op de
webinterface configureren door deze stappen te volgen:
</p>

<ul>
  <li>Log in op uw Salsa-account en ga naar de projectpagina;</li>
  <li>Klik op het bel-icoontje bovenaan de hoofdpagina van het project;</li>
  <li>Selecteer het meldingsniveau waaraan u de voorkeur geeft.</li>
</ul>
