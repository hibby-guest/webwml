#use wml::debian::template title="PO 파일이 있는 L10n Debconf 템플릿(Templates) &mdash; 번역자를 위한 힌트"
#include "$(ENGLISHDIR)/international/l10n/dtc.def"
#use wml::debian::translation-check translation="6f3adf6374f35194686f89dec2ba66b1ecf3bb5f" maintainer="Seunghun Han (kkamagui)"

<p>
#include "$(ENGLISHDIR)/international/l10n/po-debconf/menu.inc"
</p>

<h2>번역자를 위한 일반 정보</h2>

<ol>
  <li>
         번역을 시작하기 전에 debian-l10n-&lt;<em>korean</em>&gt;@lists.debian.org에
         있는 여러분의 동료 번역자와 점검하여, 현재 같은 파일을 번역하고 있는 사람은
         없는지 확인하세요.
         또, 최신 버그리포트를 읽고 여러분이 번역하려고 하는 것이 이미 제출되었는지
         보세요.
  </li>

  <li>
         번역을 업데이트하려면 항상 이전 번역자와 연락하여 중복 작업을 피하세요.
         메일 주소를 PO 파일에서 찾을 수 있을 겁니다.
  </li>

  <li>
         번역을 새로 시작하려면 <tt>templates.pot</tt> 파일을
         <tt><em>ko</em>.po</tt>로 복사하면 되고, 여기서 <em>ko</em>는 여러분 언어의
         <a href="http://www.loc.gov/standards/iso639-2/php/code_list.php">ISO 코드</a>입니다.
         그런 뒤에 첫 번째 <tt>msgstr</tt>에 번역된 문장의 문자셋을 쓰고
         유용한 정보도 넣으세요.
  </li>
  <li>
         일반적인 문서 편집기라면 어떤 것이든 PO 파일을 작업하는데
         쓸 수 있습니다. 여러분이 PO 파일이 익숙치 않다면
         <a href="https://packages.debian.org/unstable/text/gtranslator">\
         gtranslator</a>나
         <a href="https://packages.debian.org/unstable/devel/kbabel">\
         kbabel</a> 같은 유용하고 특수한 도구도 있죠. 
         <a href="https://packages.debian.org/unstable/devel/gettext">\
         gettext</a>
         <a href="https://www.gnu.org/software/gettext/manual/html_node/gettext_toc.html">\
         문서</a>는 PO 파일 형식을 설명하는데, 적어도
         <a href="https://www.gnu.org/software/gettext/manual/html_node/gettext_35.html#SEC35">\
         헤더 항목 채우기</a>와
         <a href="https://www.gnu.org/software/gettext/manual/html_node/gettext_9.html#SEC9">\
         PO 파일 형식</a> 절은 꼭 보기 바랍니다.
  </li>

  <li>
         번역을 마쳤을 때, 여러분의 파일을 적어도 한 번 다시 읽고 뜻, 철자, 문법,
         오타, 기타 실수 등을 모두 수정하세요. 여러분의 PO 편집기가 acheck와 같은
         철자 검사나 번역 도우미를 포함할 수도 있습니다.
         다음에
         <pre>
            msgfmt -c -v -o /dev/null <em>ko</em>.po
         </pre>
         명령을 돌려서 파일에 오류가 없고 문제없이 통합되는지 점검하세요.
         여러분의 언어팀 정책에 따라, 검증을 받기 위해 번역을 l10n 메일링
         리스트에 제출해야 할 수도 있습니다.
  </li>

  <li>
         여러분이 번역을 마쳤고 오류가 없을 때, 번역된 패키지에 대해 심각도(severity)를
         <a href="$(HOME)/Bugs/Developer#severities">wishlist</a>으로
         설정하여 <a href="$(HOME)/Bugs/Reporting">버그 리포트</a>를
         제출하세요.
         여러분의 리포트에 <tt>l10n</tt>과 <tt>patch</tt>
         <a href="$(HOME)/Bugs/Developer#tags">태그</a>를 붙이세요.
         그리고 여러분 언어의
         <a href="http://www.loc.gov/standards/iso639-2/php/code_list.php">ISO
         코드</a>인 <em>ko</em>을
         <tt><protect>[INTL:</protect><em>ko</em>]</tt>와 같이 제목에 추가하여
         이후에 검색을 용이하게 하세요.
         여러분의 번역이 포함될 수 있도록 정중히 부탁하고, 메인테이너에게
         번역 파일로 무엇을 해야 하는지 절차를 알려주세요. 예를 들면
         <strong>첨부 파일을 debian/po/ko.po</strong>에 복사해주세요처럼 말이죠.
         그리고 여러분의 번역을 첨부하는 것도 잊지 마세요. 이러한 과정은
         <a href="https://packages.debian.org/unstable/utils/reportbug">\
         reportbug</a> 도구를 사용하면 아주 쉽게 진행할 수 있습니다.
  </li>
</ol>

<h2>po-debconf에 국한된 정보</h2>

<ol>
  <li>
         주석의 필드 이름은 <tt>msgid</tt> 전에 표시됩니다.
         <tt>Default</tt> 값은 debconf에게 특별한데, 템플릿 형식이
         <tt>Select</tt>나 <tt>Multiselect</tt>일 때 <strong>번역 되면 안 되기</strong>
         때문입니다.
         하지만 드문 상황에서 값이 <tt>Choices</tt> 필드에 나열된 다른 문장으로
         변경되기도 합니다.
         이러한 이유로 개발자는 혼란을 막기 위해 <tt>Default</tt> 대신에
         <tt>DefaultChoice</tt>를 써야 합니다.
         <strong>이는 po-debconf 확장 기능입니다.</strong>
         debconf는 이와 같은 <tt>DefaultChoice</tt> 필드를 정의하지 않기 때문에,
         생성된 일반 템플릿 파일에는 <tt>Default</tt>가 표시될 겁니다.
  </li>

  <li>
         번역자를 위한 절차는 개발자가 넣어 두었기 때문에, 여러분이 사용하는 도구가
         이러한 주석을 망가뜨리지 않고 표시할 수 있는 것을 확신할 수 있어야 합니다.
         언어 선택을 다루는 예제는
         <a href="https://packages.debian.org/unstable/misc/geneweb">\
         geneweb</a> 패키지를 보세요.
  </li>

  <li>
         가끔 msgid가 같지만 값이 서로 다를 수 있습니다. msgid를 유일하게 만들려면
         문장의 끝에 특별한 문자를 추가하세요. 더 자세히 말하자면, 문장의 끝에
         <ul>
             <li>왼쪽 사각 괄호: <tt>[</tt></li>
             <li>공백</li>
             <li>왼쪽 사각 괄호, 오른쪽 사각 괄호, 개행을 제외한 어떤
             문자든 0개 이상 등장</li>
             <li>오른쪽 사각 괄호: <tt>]</tt></li>
         </ul>
         같은 항목이 나오면 <tt>msgid</tt> 문장에서 삭제됩니다.
         이는 역시 <tt>msgstr</tt> 문장에도 적용되므로, 번역자는 빈 문장을 넣을
         수도 있습니다.
  </li>

  <li>
         공백으로 시작하는 줄은 제외하고 debconf로 처리됩니다.
         이러한 줄은 종종 리스트 형태로 구성되지만, 각 프런트엔드는
         그만의 제약사항이 있습니다.
         모든 프런트엔드에서 비교적 잘 출력되려면 한 줄이 72 문자를
         넘지 않아야 한다는 것처럼요.
  </li>

  <li>
         <tt>podebconf-display-po</tt> 스크립트(po-debconf &gt;= 0.8.3)는
         설정 시에 debconf를 이용해서 여러분의 번역을 보여줄 수 있습니다. 여러분의
         번역이 어떻게 보이는지 대략적으로 알고 싶으면 아래의 명령어를 실행하세요.
         <pre>
            podebconf-display-po -fdialog debian/po/<em>ko</em>.po
         </pre>
         하지만, 이 방법은 기본적인 설정 파일에만 사용 가능합니다. 문자 치환을
         많이 쓰는 것과 같이 복잡한 동작이 수행되면 번역과정이 달라집니다.
         Debconf 프런트엔드에서 <tt>-f</tt> 플래그로 쓸 수 있는 옵션은
         debconf(7)에 나열되어 있습니다.
  </li>
</ol>

<podebconf-langs-short>

#include "$(ENGLISHDIR)/international/l10n/date.gen"
