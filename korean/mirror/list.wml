#use wml::debian::template title="데비안 전세계 미러 사이트" BARETITLE=true
#use wml::debian::translation-check translation="ef7400a6ea2c98aed253709dd68a428201e21d88" maintainer="Sebul" 

<p>데비안은 인터넷의 수백 서버에서 배포(<em>미러</em>)됩니다.
가까운 서버를 쓰면 다운로드 속도가 빨라질 것이고, 중심 서버 그리고 인터넷 전체의 부하와 줄일 겁니다.
</p>

<p>데비안은 일차 그리고 이차로 미러 될 수 있습니다. 정의는 아래와 같습니다:
</p>

<p class="centerblock">
  <strong>1차 미러</strong> 사이트는 대역폭이 좋고 데비안의 인 내부로 syncproxy 네트워크로부터 동기됩니다.
몇 1차 미러는<code>ftp.&lt;country&gt;.debian.org</code> 형식의 별칭을 가지므로
사용자가 기억하기 쉽습니다.
그들은 대개 모든 아키택처를 수행합니다.
</p>

<p class="centerblock">
  <strong>2차 미러</strong> 사이트는 미러하는데 (공간 제한때문에) 제한이 있을 겁니다.
사이트가 2차라고 해서 1차 사이트보다 느리거나 덜 최신일 필요는 없습니다.
사실, 2차 미러가 여러분의 아키텍처를 옮기고 여러분에게 가까우며, 그래서 더 빠르고, 거의 늘
멀리 있는 1차보다 더 좋습니다.
</p>

<p>빠른 다운로드가 가능하도록 1차건 2차건 여러분에게 가까운 사이트를 쓰세요.
<a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a> 프로그램을 써서 대기시간이 가장 짫은 사이트를 결정할 수 있습니다.
<a href="https://packages.debian.org/stable/web/wget">\
<em>wget</em></a> 또는
<a href="https://packages.debian.org/stable/net/rsync">\
<em>rsync</em></a> 같은 다운로드 프로그램을 써서 처리량이 가장 많은 사이트를 결정하세요.
지리적 근접성이 때론 어떤 기계가 여러분에게 최상의 서비스를 제공하는지에 가장 중요한 요소는 아닙니다.
</p>

<p>시스템이 많이 움직인다면 글로벌 <abbr title="Content Delivery Network">CDN</abbr>이 지원하는 "미러"를 통해 
최상의 서비스를 받을 수 있습니다.
데비안 프로젝트는 이러한 목적을 위해 deb.debian.org를 유지하며, 
당신은 이것을 적절한 source.list에서 사용할 수 있습니다. 
자세한 내용은 <a href="http://deb.debian.org/"> 웹사이트를 참조</a>하세요.

<p>다음 목록의 권한 있는 사본은 항상 다음에서 찾을 수 있습니다:
<url "https://www.debian.org/mirror/list">.
데비안 미러에 대해 여러분이 알기 바라는 모든 것:
<url "https://www.debian.org/mirror/">.
</p>

<h2 class="center">1차 데비안 미러 사이트</h2>

<table border="0" class="center">
<tr>
  <th>나라</th>
  <th>사이트</th>
  <th>아키텍처</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">데비안 아카이브의 2차 미러</h2>

<table border="0" class="center">
<tr>
  <th>Host 이름</th>
  <th>HTTP</th>
  <th>아키텍처</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
