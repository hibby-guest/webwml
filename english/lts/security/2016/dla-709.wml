<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
  <p>Several bugs were discovered in PostgreSQL, a relational database server
  system. This update corrects various stability issues.</p>

  <p>9.1.24 marks the end of life of the PostgreSQL 9.1 branch. No further
  releases will be made by the PostgreSQL Global Development Group.</p>

  <p>Users of PostgreSQL 9.1 should look into upgrading to a newer PostgreSQL
  release. Options are:</p>

<ul>

  <li>Upgrading to Debian 8 (Jessie), providing postgresql-9.4.</li>

  <li><p>The use of the apt.postgresql.org repository, providing packages for all
    active PostgreSQL branches (9.2 up to 9.6 at the time of writing).</p>

    <p>See <a href="https://wiki.postgresql.org/wiki/Apt">https://wiki.postgresql.org/wiki/Apt</a> for more information about the
    repository.</p>

    <p>A helper script to activate the repository is provided in
    /usr/share/doc/postgresql-9.1/examples/apt.postgresql.org.sh.gz.</p></li>

  <li><p>In Debian, an LTS version of 9.1 is in planning that will cover the
    lifetime of wheezy-lts. Updates will made on a best-effort basis. Users
    can take advantage of this, but should still consider upgrading to newer
    PostgreSQL versions over the next months.</p>

    <p>See <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a> for more information about Debian LTS.</p></li>

</ul>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in postgresql-9.1 version 9.1.24-0+deb7u1</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-709.data"
# $Id: $
