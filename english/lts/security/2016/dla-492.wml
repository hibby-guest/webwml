<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the maintainer scripts of pdns-backend-mysql
grant too wide database permissions for the pdns user. Other backends
are not affected.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.1-4.1+deb7u1.</p>

<p>Note that if you're running the pdns server with the mysql backend these
additional grants are not removed automatically since they might have
been added on purpose. Check</p>

    <p>/usr/share/doc/pdns-backend-mysql/NEWS.Debian.gz</p>

<p>on how to remove them.</p>

<p>We recommend that you upgrade your pdns packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-492.data"
# $Id: $
