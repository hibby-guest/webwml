<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been discovered in the MySQL database server. The
vulnerabilities are addressed by upgrading MySQL to the new upstream
version 5.5.53, which includes additional changes, such as performance
improvements, bug fixes, new features, and possibly incompatible
changes. Please see the MySQL 5.5 Release Notes and Oracle's Critical
Patch Update advisory for further details:</p>

<ul>
 <li><a href="https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-53.html">https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-53.html</a></li>
 <li><a href="http://www.oracle.com/technetwork/security-advisory/cpuoct2016-2881722.html">http://www.oracle.com/technetwork/security-advisory/cpuoct2016-2881722.html</a></li>
</ul>

<p>Also note that packaging will now create /var/lib/mysql-files, as
server will now by default restrict all import/export operations to
this directory.This can be changed using the secure-file-priv
configuration option.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
5.5.53-0+deb7u1.</p>

<p>We recommend that you upgrade your mysql-5.5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-708.data"
# $Id: $
