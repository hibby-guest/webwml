<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that an invalid XML file can trigger an out-of-bound
memory access in Pidgin, a multi-protocol instant messaging client,
when it is sent by a malicious server. This might lead to a crash or,
in some extreme cases, to remote code execution in the client-side.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.10.10-1~deb7u3.</p>

<p>We recommend that you upgrade your pidgin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-853.data"
# $Id: $
