<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there two vulnerabilities the library providing
font selection and rasterisation, libxfont:</p>

<ul>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13720">CVE-2017-13720</a>
    <p>If a pattern contained a '?' character any character
    in the string is skipped even if it was a '\0'. The rest of the
    matching then read invalid memory.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2017-13722">CVE-2017-13722</a>
    <p>A malformed PCF file could cause the library to make
    reads from random heap memory that was behind the `strings` buffer,
    leading to an application crash or a information leak.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in libxfont version
1:1.4.5-5+deb7u1.</p>

<p>We recommend that you upgrade your libxfont packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1126.data"
# $Id: $
