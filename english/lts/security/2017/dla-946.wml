<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The NSS library is vulnerable to two security issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5461">CVE-2017-5461</a>

    <p>Out-of-bounds write in Base64 encoding. This can trigger a crash
    (denial of service) and might be exploitable for code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5462">CVE-2017-5462</a>

    <p>A flaw in DRBG number generation where the internal state V does not
    correctly carry bits over.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:3.26-1+debu7u3.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-946.data"
# $Id: $
