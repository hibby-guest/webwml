<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that an out of bounds write caused by a heap-based buffer
overflow could be triggered in freetype via a crafted font.</p>

<p>This update also reverts the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2016-10328">CVE-2016-10328</a>, as it was
determined that freetype 2.4.9 is not affected by that issue.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.4.9-1.1+deb7u6.</p>

<p>We recommend that you upgrade your freetype packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-918.data"
# $Id: $
