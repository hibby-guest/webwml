<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in the PostgreSQL database
system:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7486">CVE-2017-7486</a>

    <p>Andrew Wheelwright discovered that user mappings were insufficiently
    restricted.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7546">CVE-2017-7546</a>

    <p>In some authentication methods empty passwords were accepted.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7547">CVE-2017-7547</a>

    <p>User mappings could leak data to unprivileged users.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
9.1.24lts2-0+deb7u1.</p>

<p>We recommend that you upgrade your postgresql-9.1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1051.data"
# $Id: $
