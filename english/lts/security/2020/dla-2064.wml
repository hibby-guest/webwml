<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that a hook script of ldm, the display manager for the
Linux Terminal Server Project incorrectly parsed responses from an SSH server
which could result in local root privilege escalation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20373">CVE-2019-20373</a>

    <p>LTSP LDM through 2.18.06 allows fat-client root access because the
    <tt>LDM_USERNAME</tt> variable may have an empty value if the user's shell
    lacks support for Bourne shell syntax. This is related to a
    <tt>run-x-session</tt> script.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2:2.2.15-2+deb8u1.</p>

<p>We recommend that you upgrade your ldm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2064.data"
# $Id: $
