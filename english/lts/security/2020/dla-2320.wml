<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A process running under a restrictive seccomp filter that specified multiple syscall arguments could bypass intended access restrictions by specifying a single matching argument. runc has been rebuilt with the fixed package.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.0~git20150813.0.1b506fc-2+deb9u1.</p>

<p>We recommend that you upgrade your golang-github-seccomp-libseccomp-golang and runc packages, and recompile own Go code using golang-github-seccomp-libseccomp-golang.</p>

<p>For the detailed security status of golang-github-seccomp-libseccomp-golang please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-github-seccomp-libseccomp-golang">https://security-tracker.debian.org/tracker/golang-github-seccomp-libseccomp-golang</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2320.data"
# $Id: $
