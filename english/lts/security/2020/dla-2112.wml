<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that ReportLab, a Python library to create PDF documents,
did not properly parse color strings, allowing an attacker to execute
arbitrary code through a crafted input document.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.1.8-3+deb8u2.</p>

<p>We recommend that you upgrade your python-reportlab packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2112.data"
# $Id: $
