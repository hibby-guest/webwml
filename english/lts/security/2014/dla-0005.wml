<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jakub Wilk discovered that APT, the high level package manager,
did not properly perform authentication checks for source packages
downloaded via "apt-get source". This only affects use cases where
source packages are downloaded via this command; it does not
affect regular Debian package installation and upgrading.
(<a href="https://security-tracker.debian.org/tracker/CVE-2014-0478">CVE-2014-0478</a>)</p>

<p>It was discovered that APT incorrectly handled the Verify-Host
configuration option. If a remote attacker were able to perform a
man-in-the-middle attack, this flaw could potentially be used to steal
repository credentials. This only relevant for systems that use APT
sources on https connections (requires the apt-transport-https package
to be installed). (<a href="https://security-tracker.debian.org/tracker/CVE-2011-3634">CVE-2011-3634</a>)</p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in apt version 0.8.10.3+squeeze2</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2014/dla-0005.data"
# $Id: $
