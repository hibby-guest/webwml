<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0195">CVE-2014-0195</a>

    <p>Jueri Aedla discovered that a buffer overflow in processing DTLS
    fragments could lead to the execution of arbitrary code or denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0221">CVE-2014-0221</a>

    <p>Imre Rad discovered the processing of DTLS hello packets is
    susceptible to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0224">CVE-2014-0224</a>

    <p>KIKUCHI Masashi discovered that carefully crafted handshakes can
    force the use of weak keys, resulting in potential man-in-the-middle
    attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3470">CVE-2014-3470</a>

    <p>Felix Groebert and Ivan Fratric discovered that the implementation of
    anonymous ECDH ciphersuites is suspectible to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0076">CVE-2014-0076</a>

     <p>Fix for the attack described in the paper "Recovering
     OpenSSL ECDSA Nonces Using the FLUSH+RELOAD Cache Side-channel Attack"
     Reported by Yuval Yarom and Naomi Benger.</p></li>

</ul>

<p>Additional information can be found at
<a href="http://www.openssl.org/news/secadv_20140605.txt">http://www.openssl.org/news/secadv_20140605.txt</a></p>

<p>All applications linked to openssl need to be restarted. You can
use the tool checkrestart from the package debian-goodies to
detect affected programs or reboot your system.</p>

<p>It's important that you upgrade the libssl0.9.8 package and not
just the openssl package.</p>

<p>For Debian 6 <q>Squeeze</q>, these issues have been fixed in openssl version 0.9.8o-4squeeze15</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2014/dla-0003.data"
# $Id: $
