<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a NULL pointer dereference issue
in the IW44 encoder/decoder within DjVu, a set of compression
technologies for high-resolution images.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18804">CVE-2019-18804</a>

    <p>DjVuLibre 3.5.27 has a NULL pointer dereference in the function DJVU::filter_fv at IW44EncodeCodec.cpp.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.5.25.4-4+deb8u2.</p>

<p>We recommend that you upgrade your djvulibre packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1985.data"
# $Id: $
