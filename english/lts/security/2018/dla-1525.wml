<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7653">CVE-2017-7653</a>

     <p>As invalid UTF-8 strings are not correctly checked, an attacker could
     cause a denial of service to other clients by disconnecting
     them from the broker with special crafted topics.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7654">CVE-2017-7654</a>

     <p>Due to a memory leak unauthenticated clients can send special crafted
     CONNECT packets which could cause a denial of service in the broker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9868">CVE-2017-9868</a>

     <p>Due to wrong file permissions local users could obtain topic
     information from the mosquitto database.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.4-2+deb8u3.</p>

<p>We recommend that you upgrade your mosquitto packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1525.data"
# $Id: $
