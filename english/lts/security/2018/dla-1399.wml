<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two flaws were discovered in ruby-passenger for Ruby Rails and Rack
support that allowed attackers to spoof HTTP headers or exploit a race
condition which made privilege escalation under certain conditions
possible.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7519">CVE-2015-7519</a>

    <p>Remote attackers could spoof headers passed to applications by using
    an underscore character instead of a dash character in an HTTP
    header as demonstrated by an X_User header.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12029">CVE-2018-12029</a>

    <p>A vulnerability was discovered by the Pulse Security team. It was
    exploitable only when running a non-standard
    passenger_instance_registry_dir, via a race condition where after a
    file was created, there was a window in which it could be replaced
    with a symlink before it was chowned via the path and not the file
    descriptor. If the symlink target was to a file which would be
    executed by root such as root's crontab file, then privilege
    escalation was possible. This is now mitigated by using fchown().</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.0.53-1+deb8u1.</p>

<p>We recommend that you upgrade your ruby-passenger packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1399.data"
# $Id: $
