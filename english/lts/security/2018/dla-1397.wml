<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were found in PHP, a widely-used open source
general purpose scripting language:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7584">CVE-2018-7584</a>

<p>A stack-buffer-overflow while parsing HTTP response results in copying a
large string and possible memory corruption and/or denial of service</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10545">CVE-2018-10545</a>

<p>Dumpable FPM child processes allow bypassing opcache access controls
resulting in potential information disclosure where one user can obtain
information about another user's running PHP applications</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10546">CVE-2018-10546</a>

<p>An invalid sequence of bytes can trigger an infinite loop in the stream
filter convert.iconv</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10547">CVE-2018-10547</a>

<p>A previous fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-5712">CVE-2018-5712</a> may not be complete, resulting in an
additional vulnerability in the form of a reflected XSS in the PHAR 403
and 404 error pages</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10548">CVE-2018-10548</a>

<p>A malicious remote LDAP server can send a crafted response that will
cause a denial of service (NULL pointer dereference resulting in an
application crash)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10549">CVE-2018-10549</a>

<p>A crafted JPEG file can case an out-of-bounds read and heap buffer
overflow</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
5.6.36+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1397.data"
# $Id: $
