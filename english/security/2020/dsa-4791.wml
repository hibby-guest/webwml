<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Ken Gaillot discovered a vulnerability in the Pacemaker cluster
resource manager: If ACLs were configured for users in the <q>haclient</q>
group, the ACL restrictions could be bypassed via unrestricted IPC
communication, resulting in cluster-wide arbitrary code execution with
root privileges.</p>

<p>If the <q>enable-acl</q> cluster option isn't enabled, members of the
<q>haclient</q> group can modify Pacemaker's Cluster Information Base without
restriction, which already gives them these capabilities, so there is
no additional exposure in such a setup.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 2.0.1-5+deb10u1.</p>

<p>We recommend that you upgrade your pacemaker packages.</p>

<p>For the detailed security status of pacemaker please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pacemaker">\
https://security-tracker.debian.org/tracker/pacemaker</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4791.data"
# $Id: $
