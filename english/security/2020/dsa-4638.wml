<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19880">CVE-2019-19880</a>

    <p>Richard Lorenz discovered an issue in the sqlite library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19923">CVE-2019-19923</a>

    <p>Richard Lorenz discovered an out-of-bounds read issue in the sqlite
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19925">CVE-2019-19925</a>

    <p>Richard Lorenz discovered an issue in the sqlite library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19926">CVE-2019-19926</a>

    <p>Richard Lorenz discovered an implementation error in the sqlite library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6381">CVE-2020-6381</a>

    <p>UK's National Cyber Security Centre discovered an integer overflow issue
    in the v8 javascript library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6382">CVE-2020-6382</a>

    <p>Soyeon Park and Wen Xu discovered a type error in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6383">CVE-2020-6383</a>

    <p>Sergei Glazunov discovered a type error in the v8 javascript library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6384">CVE-2020-6384</a>

    <p>David Manoucheri discovered a use-after-free issue in WebAudio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6385">CVE-2020-6385</a>

    <p>Sergei Glazunov discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6386">CVE-2020-6386</a>

    <p>Zhe Jin discovered a use-after-free issue in speech processing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6387">CVE-2020-6387</a>

    <p>Natalie Silvanovich discovered an out-of-bounds write error in the WebRTC
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6388">CVE-2020-6388</a>

    <p>Sergei Glazunov discovered an out-of-bounds read error in the WebRTC
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6389">CVE-2020-6389</a>

    <p>Natalie Silvanovich discovered an out-of-bounds write error in the WebRTC
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6390">CVE-2020-6390</a>

    <p>Sergei Glazunov discovered an out-of-bounds read error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6391">CVE-2020-6391</a>

    <p>Michał Bentkowski discoverd that untrusted input was insufficiently
    validated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6392">CVE-2020-6392</a>

    <p>The Microsoft Edge Team discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6393">CVE-2020-6393</a>

    <p>Mark Amery discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6394">CVE-2020-6394</a>

    <p>Phil Freo discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6395">CVE-2020-6395</a>

    <p>Pierre Langlois discovered an out-of-bounds read error in the v8
    javascript library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6396">CVE-2020-6396</a>

    <p>William Luc Ritchie discovered an error in the skia library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6397">CVE-2020-6397</a>

    <p>Khalil Zhani discovered a user interface error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6398">CVE-2020-6398</a>

    <p>pdknsk discovered an uninitialized variable in the pdfium library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6399">CVE-2020-6399</a>

    <p>Luan Herrera discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6400">CVE-2020-6400</a>

    <p>Takashi Yoneuchi discovered an error in Cross-Origin Resource Sharing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6401">CVE-2020-6401</a>

    <p>Tzachy Horesh discovered that user input was insufficiently validated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6402">CVE-2020-6402</a>

    <p>Vladimir Metnew discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6403">CVE-2020-6403</a>

    <p>Khalil Zhani discovered a user interface error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6404">CVE-2020-6404</a>

    <p>kanchi discovered an error in Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6405">CVE-2020-6405</a>

    <p>Yongheng Chen and Rui Zhong discovered an out-of-bounds read issue in the
    sqlite library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6406">CVE-2020-6406</a>

    <p>Sergei Glazunov discovered a use-after-free issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6407">CVE-2020-6407</a>

    <p>Sergei Glazunov discovered an out-of-bounds read error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6408">CVE-2020-6408</a>

    <p>Zhong Zhaochen discovered a policy enforcement error in Cross-Origin
    Resource Sharing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6409">CVE-2020-6409</a>

    <p>Divagar S and Bharathi V discovered an error in the omnibox
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6410">CVE-2020-6410</a>

    <p>evil1m0 discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6411">CVE-2020-6411</a>

    <p>Khalil Zhani discovered that user input was insufficiently validated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6412">CVE-2020-6412</a>

    <p>Zihan Zheng discovered that user input was insufficiently validated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6413">CVE-2020-6413</a>

    <p>Michał Bentkowski discovered an error in Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6414">CVE-2020-6414</a>

    <p>Lijo A.T discovered a policy safe browsing policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6415">CVE-2020-6415</a>

    <p>Avihay Cohen discovered an implementation error in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6416">CVE-2020-6416</a>

    <p>Woojin Oh discovered that untrusted input was insufficiently validated.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6418">CVE-2020-6418</a>

    <p>Clement Lecigne discovered a type error in the v8 javascript library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6420">CVE-2020-6420</a>

    <p>Taras Uzdenov discovered a policy enforcement error.</p></li>

</ul>

<p>For the oldstable distribution (stretch), security support for chromium has
been discontinued.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 80.0.3987.132-1~deb10u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4638.data"
# $Id: $
