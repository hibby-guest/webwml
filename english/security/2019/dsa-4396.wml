<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in Ansible, a configuration
management, deployment, and task execution system:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10855">CVE-2018-10855</a>
/ <a href="https://security-tracker.debian.org/tracker/CVE-2018-16876">CVE-2018-16876</a>

    <p>The no_log task flag wasn't honored, resulting in an information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10875">CVE-2018-10875</a>

    <p>ansible.cfg was read from the current working directory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16837">CVE-2018-16837</a>

    <p>The user module leaked parameters passed to ssh-keygen to the process
    environment.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3828">CVE-2019-3828</a>

    <p>The fetch module was susceptible to path traversal.</p></li>

</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 2.2.1.0-2+deb9u1.</p>

<p>We recommend that you upgrade your ansible packages.</p>

<p>For the detailed security status of ansible please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ansible">https://security-tracker.debian.org/tracker/ansible</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4396.data"
# $Id: $
