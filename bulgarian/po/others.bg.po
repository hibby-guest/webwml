#
# Damyan Ivanov <dmn@debian.org>, 2010-2020.
#
msgid ""
msgstr ""
"Project-Id-Version: others\n"
"Report-Msgid-Bugs-To: debian-www@lists.debian.org\n"
"POT-Creation-Date: 2010-12-16 22:41+0300\n"
"PO-Revision-Date: 2020-12-17 14:35+0200\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Bulgarian <dict@ludost.net>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Generator: Gtranslator 3.38.0\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Кът за нови членове"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Стъпка 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Стъпка 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Стъпка 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Стъпка 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Стъпка 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Стъпка 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Стъпка 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Списък със задачи на кандидатстващия"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"За повече информация прочетете <a href=\"m4_HOME/intl/french/\">http://www."
"debian.org/intl/french/</a> (достъпен само на френски)."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Повече информация"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"За повече информация прочетете <a href=\"m4_HOME/intl/spanish/\">http://www."
"debian.org/intl/spanish/</a> (достъпен само на инспански)."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Телефон"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Факс"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Адрес"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Продукти"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "тениски"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "шапки"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "стикери"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "чаши"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "други дрехи"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "блузи тип поло"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "фризби"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "подложки за мишки"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "баджове"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "баскетболни табла"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "обици"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "чанти"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "чадъри"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "калъфки за възглавници"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "ключодържатели"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "Швейцарски армейски ножчета"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "Носители USB"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "ленти за прикрепване на бадж"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "други"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr "Достъпни езици:"

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr "Доставка в чужбина:"

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr "в Европа"

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr "Страна на произход:"

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr "Дарител на парични средства"

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""
"Парични дарения, използвани за организиране на местни събития, свързани със "
"свободен софтуер"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "С&nbsp;``Debian''"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Без&nbsp;``Debian''"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Капсулован PostScrip"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Задвижван от Дебиан]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Задвижван от Дебиан ГНУ/Линукс]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "Задвижван от Дебиан"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Дебиан] (малък бутон)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "също като по-горе"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "От колко време използвате Дебиан?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Член ли сте на проекта Дебиан?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "В кои сфери на Дебиан са интересите ви?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Какво ви подтикна да работите с Дебиан?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr "Имате ли съвети към жените, желаещи да помогнат на Дебиан?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr "Работите ли заедно с други жени в група с техническа насоченост? Кои?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Повече за вас…"

#~ msgid "Download"
#~ msgstr "Изтегляне"

#~ msgid "Old banner ads"
#~ msgstr "Стари рекламни банери"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "BAD"
#~ msgstr "ЛОШ"

#~ msgid "OK?"
#~ msgstr "OK?"

#~ msgid "BAD?"
#~ msgstr "ЛОШ?"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "Unknown"
#~ msgstr "Непознат"

#~ msgid "ALL"
#~ msgstr "ВСИЧКИ"

#~ msgid "Package"
#~ msgstr "Пакет"

#~ msgid "Status"
#~ msgstr "Статус"

#~ msgid "Version"
#~ msgstr "Версия"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "p<get-var page />"
#~ msgstr "p<get-var page />"

#~ msgid "Previous Talks:"
#~ msgstr "Предишни разговори:"

#~ msgid "Location:"
#~ msgstr "Локация:"

#~ msgid "Topics:"
#~ msgstr "Теми:"

#~ msgid "closed"
#~ msgstr "затворен"

#~ msgid "open"
#~ msgstr "отворен"

#~ msgid "Unsubscribe"
#~ msgstr "Отписване"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "Моля изберете за кои списъци желаете да се отпишете:"

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription "
#~ "web form</a> is also available, for subscribing to mailing lists. "
#~ msgstr ""
#~ "Прочетете страницата за <a href=\"./#subunsub\">пощенски списъци</a> за "
#~ "информация как да се отпишете от тях чрез e-mail. Достъпна е и <a href="
#~ "\"unsubscribe\">web форма за абониране</a> за пощенски списъци"

#~ msgid "Mailing List Unsubscription"
#~ msgstr "Отписване от пощенски списъци"

#~ msgid ""
#~ "Please respect the <a href=\"./#ads\">Debian mailing list advertising "
#~ "policy</a>."
#~ msgstr ""
#~ "Моля съобразявайте се с  <a href=\"./#ads\">правилник за рекламиране в "
#~ "пощенските списъци на Дебиан</a>."

#~ msgid "Clear"
#~ msgstr "Изчистване"

#~ msgid "Subscribe"
#~ msgstr "Абониране"

#~ msgid "Your E-Mail address:"
#~ msgstr "Вашият E-Mail адрес:"

#~ msgid "is a read-only, digestified version."
#~ msgstr "е само за четене, версия дайджест"

#~ msgid "Subscription:"
#~ msgstr "Абониран:"

#~ msgid ""
#~ "Only messages signed by a Debian developer will be accepted by this list."
#~ msgstr ""
#~ "Само съобщения, подписани от Debian програмист ще бъдат приемани в този "
#~ "списък."

#~ msgid "Posting messages allowed only to subscribers."
#~ msgstr "Пускането на съобщения е разрешено само за абонирани."

#~ msgid "Moderated:"
#~ msgstr "Модериран:"

#~ msgid "No description given"
#~ msgstr "Без описание"

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Моля изберете за кои списъци желаете да се абонирате:"

#~ msgid ""
#~ "Note that most Debian mailing lists are public forums. Any mails sent to "
#~ "them will be published in public mailing list archives and indexed by "
#~ "search engines. You should only subscribe to Debian mailing lists using "
#~ "an e-mail address that you do not mind being made public."
#~ msgstr ""
#~ "Имайте предвид, че повечето пощенски списъци на Debian са публични. Всеки "
#~ "email изпратен до тях ще бъде публикуван в публичен пощенски архив и ще "
#~ "бъде индексиран от търсещи машини. Затова използвайте email адрес, който "
#~ "може да стане публично достояние."

#~ msgid ""
#~ "See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
#~ "how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription "
#~ "web form</a> is also available, for unsubscribing from mailing lists. "
#~ msgstr ""
#~ "Прочетете страницата за <a href=\"./#subunsub\">пощенски списъци</a> за "
#~ "информация как да се абонирате за тях чрез e-mail. Достъпна е и <a href="
#~ "\"unsubscribe\">web форма за отписване</a> от пощенските списъци"

#~ msgid "Mailing List Subscription"
#~ msgstr "Абониране за пощенски списък"

#~ msgid ""
#~ "<total_consultant> Debian consultants listed in <total_country> countries "
#~ "worldwide."
#~ msgstr ""
#~ "<total_consultant> Debian консултанти от <total_country> страни от цял "
#~ "свят."

#~ msgid "Willing to Relocate"
#~ msgstr "Желае да се премества"

#~ msgid "Rates:"
#~ msgstr "Рейтинг:"

#~ msgid "Email:"
#~ msgstr "Email:"

#~ msgid "or"
#~ msgstr "или"

#~ msgid "URL:"
#~ msgstr "URL:"

#~ msgid "Company:"
#~ msgstr "Фирма:"

#~ msgid "Name:"
#~ msgstr "Име:"

#~ msgid "Where:"
#~ msgstr "Къде:"

#~ msgid "Specifications:"
#~ msgstr "Спецификации:"

#~ msgid "Architecture:"
#~ msgstr "Архитектура:"

#~ msgid "Who:"
#~ msgstr "Кой:"

#~ msgid "Wanted:"
#~ msgstr "Желани:"

#~ msgid "Unavailable"
#~ msgstr "Недостъпен"

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "<void id=\"d-i\" />Неясен"

#~ msgid "No images"
#~ msgstr "Няма файлове-образи"

#~ msgid "No kernel"
#~ msgstr "Няма ядро"

#~ msgid "Not yet"
#~ msgstr "Още не"

#~ msgid "Building"
#~ msgstr "Компилира се"

#~ msgid "Booting"
#~ msgstr "Зарежда"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (неработещ)"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "Working"
#~ msgstr "Работи"
